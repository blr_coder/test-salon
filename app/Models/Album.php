<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'date',
        'image_for_desktop',
        'image_for_desktop_small',
        'image_for_mobile',
        'photographer',
        'location',
    ];

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
}
