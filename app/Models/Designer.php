<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Designer extends Model
{
    protected $fillable = [
        'title',
        'slug',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
