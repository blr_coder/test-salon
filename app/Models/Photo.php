<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'album_id',
        'image',
        'image_small',
        'title',
    ];

    public function album()
    {
        return $this->belongsTo(Album::class);
    }
}
