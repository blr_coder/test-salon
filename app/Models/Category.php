<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *@property int $id
 *@property string $title
 *@property string $slug
 *@property int|null $parent_id
 *@property int|null $number
 */

class Category extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'parent_id',
        'number',
    ];

    /** Scope */

    public function scopeMain($query)
    {
        return $query->where('parent_id', 0);
    }

    public function scopeSecondary($query)
    {
        return $query->where('parent_id', '!=', 0);
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function isMain()
    {
        return $this->parent_id == 0;
    }

    public function delete()
    {
        foreach (Category::where('parent_id', $this->id)->get() as $category)
            $category->delete();

        foreach (Product::where('category_id', $this->id)->get() as $product)
            $product->delete();

        return parent::delete();
    }
}
