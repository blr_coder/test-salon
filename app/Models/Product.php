<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    protected $casts = [
        'sizes' => 'array',
    ];

    protected $fillable = [
        'title',
        'slug',
        'model',
        'description',

        'price',
        'discount_price',

        'image_1',
        'image_2',
        'image_3',

        'video',
        'status',

        'new_or_special_price',

        'sizes',
        'colors',

        'category_id',
        'designer_id',

        'show_recommendations',
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function isNew(): bool {
        return $this->new_or_special_price === 1;
    }

    public function isSpecialPrice(): bool {
        return $this->new_or_special_price === 2;
    }

    public function delete()
    {
        if (!empty($this->image_1)) Storage::delete('/public/uploaded_images/products/' . $this->image_1);
        if (!empty($this->image_2)) Storage::delete('/public/uploaded_images/products/' . $this->image_2);
        if (!empty($this->image_3)) Storage::delete('/public/uploaded_images/products/' . $this->image_3);

        return parent::delete();
    }
}
