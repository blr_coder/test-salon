<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title',
        'preview',
        'description',
        'date',
        'image_for_desktop',
        'image_for_desktop_small',
        'image_for_mobile',
        'slug'
    ];
}
