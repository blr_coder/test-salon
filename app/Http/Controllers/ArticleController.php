<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy('date', 'desc')->get();
        return view('articles.index', compact('articles'))
            ->with('page_title', 'Новости салона вечерней моды Sensation')
            ->with('page_description', 'Самые актуальные новости из мира моды и стиля');
    }

    public function show($slug)
    {
        $article = Article::where('slug', $slug)->first();
        $otherArticles = Article::whereNotIn('slug', [$slug])->get();

        return view('articles.show', compact('article', 'otherArticles'))
            ->with('page_title', $article->title)
            ->with('page_description', $article->preview);
    }
}
