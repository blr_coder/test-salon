<?php

namespace App\Http\Controllers;

use App\Models\Album;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    public function index()
    {
        $albums = Album::orderBy('date', 'desc')->get();
        return view('photo_sessions.index', compact('albums'))
            ->with('page_title', 'Фотосессии платьев салона вечерней моды Sensation')
            ->with('page_description', 'Фотосессии платьев салона вечерней моды Sensation');
    }

    public function show($slug)
    {
        $album = Album::where('slug', $slug)->first();
        return view('photo_sessions.show', compact('album'))
            ->with('page_title', $album->title)
            ->with('page_description', $album->preview);
    }
}
