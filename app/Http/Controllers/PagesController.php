<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        return view('index')
            ->with('page_title', 'Салон вечерней моды - Sensation')
            ->with('page_description', 'Salon Sensation - салон вечерних и свадебных платьев в Минске. Продажа вечерних, выпускных и коктейльных платьев.');
    }

    public function about() {
        return view('info.about')
            ->with('page_title', 'О салоне вечерней моды - Sensation')
            ->with('page_description', 'Salon Sensation - мультибрендовый бутик эксклюзивных платьев от дизайнеров с мировым именем!. Продажа вечерних, выпускных и коктейльных платьев в Минске.');
    }
}
