<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryApiController extends Controller
{
    public function getMainCategories()
    {
//        $categories = Category::query()->get();
//        $categories = Category::query()->where('parent_id', 0)->get();
        $mainCategories = Category::query()->main()->get();
        foreach ($mainCategories as $mainCategory)
            if (!empty($mainCategory->subcategories))
                foreach ($mainCategory->subcategories as $subcategory) {
                    $mainCategory->push($subcategory);
                }
//        return $mainCategories;
        return CategoryResource::collection($mainCategories);
    }


    public function getSubCategories()
    {
        $subCategories = Category::query()->Secondary()->get();
        return CategoryResource::collection($subCategories);
    }
}
