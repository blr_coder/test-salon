<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DesignerResource;
use App\Models\Designer;
use Illuminate\Http\Request;

class DesignerApiController extends Controller
{
    public function get()
    {
        $designers = Designer::query()->get();
        return DesignerResource::collection($designers);
    }
}
