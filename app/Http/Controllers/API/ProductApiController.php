<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductApiController extends Controller
{
    public function get()
    {
        /*$query = Product::query();
        $products = $query->get();*/

        $products = Product::query()->get();
        return ProductResource::collection($products);
    }
}
