<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Photos\CreateRequest;
use App\Models\Album;
use App\Models\Photo;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PhotoController extends Controller
{
    public function index(Album $album)
    {
        return view('Admin.photos.index', compact('album'));
    }

    public function store(CreateRequest $request, Album $album, ImageService $imageService)
    {
//        dd($request->all());
        foreach ($request->images as $image) {
            $imgData = array();

            $imgData += ['image' => $imageService->saveImage($image, 'photos', 1920)];
            $imgData += ['image_small' => $imageService->saveImage($image, 'photos/small', 505, 350)];
            $imgData += ['title' => $request->title];
            $imgData += ['album_id' => $album->id];

            Photo::create($imgData);
        }

        return back()->with('flash_message', 'Выбранные фото загружены!');
    }

    public function delete(Photo $photo)
    {
        if (!empty($photo->image)) Storage::delete('/public/uploaded_images/photos/' . $photo->image);
        if (!empty($photo->image_small)) Storage::delete('/public/uploaded_images/photos/small/' . $photo->image_small);

        $photo->delete();

        return back()->with('flash_message', 'Фото удалено!!!!');
    }

    public function deleteAll(Album $album)
    {
        foreach ($album->photos as $photo)
        {
            if (!empty($photo->image)) Storage::delete('/public/uploaded_images/photos/' . $photo->image);
            if (!empty($photo->image_small)) Storage::delete('/public/uploaded_images/photos/small/' . $photo->image_small);
            $photo->delete();
        }

        return back()->with('flash_message', 'Все фото удалены!!!!');
    }
}
