<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Albums\CreateRequest;
use App\Http\Requests\Admin\Albums\UpdateRequest;
use App\Models\Album;
use App\Services\ImageService;
use App\Services\SlugService;
use Illuminate\Support\Facades\Storage;

class AlbumController extends Controller
{
    public function index()
    {
        $albums = Album::orderBy('date', 'desc')->get();
        return view('Admin.albums.index', compact('albums'));
    }

    public function create()
    {
        return view('Admin.albums.create');
    }

    public function store(CreateRequest $request, ImageService $imageService, SlugService $slugService)
    {
        $data = $request->all();

        $data += ['slug' => $slugService->makeSlug($data['title'], 'albums')];
        $data += ['image_for_desktop' => $imageService->saveImage($request->file('desktop_image_file'), 'albums', 1920, 830)];
        $data += ['image_for_desktop_small' => $imageService->saveImage($request->file('desktop_image_file'), 'albums/small', 505, 350)];
        $data += ['image_for_mobile' => $imageService->saveImage($request->file('mobile_image_file'), 'albums/mobile', 250, 120)];

        $album = Album::create($data);

        return redirect('/admin/albums')->with('flash_message', "Фотосессия {$album->title} добавлена!");
    }

    public function edit(Album $album)
    {
        return view('Admin.albums.edit', compact('album'));
    }

    public function update(UpdateRequest $request, ImageService $imageService, Album $album)
    {
        $data = $request->all();

        if (!empty($request->file('desktop_image_file')))
        {
            if (!empty($album->image_for_desktop)) Storage::delete('/public/uploaded_images/albums/' . $album->image_for_desktop);
            if (!empty($album->image_for_desktop_small)) Storage::delete('/public/uploaded_images/albums/small/' . $album->image_for_desktop_small);

            $data += ['image_for_desktop' => $imageService->saveImage($request->file('desktop_image_file'), 'albums', 1920, 830)];
            $data += ['image_for_desktop_small' => $imageService->saveImage($request->file('desktop_image_file'), 'albums/small', 505, 350)];
        }

        if (!empty($request->file('mobile_image_file')))
        {
            if (!empty($album->image_for_mobile)) Storage::delete('/public/uploaded_images/albums/mobile/' . $album->image_for_mobile);
            $data += ['image_for_mobile' => $imageService->saveImage($request->file('mobile_image_file'), 'albums/mobile', 250, 120)];
        }

        $old_name = $album->title;
        $album->update($data);

        return redirect('/admin/albums')->with('flash_message', "Фотосессия {$old_name} обновлена!");
    }

    public function delete(Album $album)
    {
        if (!empty($album->image_for_desktop)) Storage::delete('/public/uploaded_images/albums/' . $album->image_for_desktop);
        if (!empty($album->image_for_desktop_small)) Storage::delete('/public/uploaded_images/albums/small/' . $album->image_for_desktop_small);
        if (!empty($album->image_for_mobile)) Storage::delete('/public/uploaded_images/albums/mobile/' . $album->image_for_mobile);

        $album->delete();
        return redirect('/admin/albums')->with('flash_message', "Фотосессия {$album->title} удалена!");
    }
}
