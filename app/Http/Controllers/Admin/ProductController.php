<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Products\CreateRequest;
use App\Http\Requests\Admin\Products\UpdateRequest;
use App\Models\Category;
use App\Models\Designer;
use App\Models\Product;
use App\Services\ImageService;
use App\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $query = Product::query();

        if (!empty($value = $request->get('id'))){
            $query = $query->where('id', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('title'))){
            $query = $query->where('title', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('model'))){
            $query = $query->where('model', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('designer'))){
            $designers_id = Designer::where('title', 'like', '%' . $value . '%')->select('id')->get()->toArray();
            $query = $query->whereIn('designer_id', $designers_id);
        }

        if (!empty($value = $request->get('category'))){
            $categories_id = Category::where('title', 'like', '%' . $value . '%')->select('id')->get()->toArray();
            $query = $query->whereIn('category_id', $categories_id);
        }

        $products = $query->latest()->get();

//        $products = Product::orderBy('status')->get();
        return view('Admin.products.index', compact('products'));
    }

    public function create()
    {
        $designers = Designer::all();
        $categories = Category::main()->orderBy('title')->get();
        return view('Admin.products.create', compact('designers', 'categories'));
    }

    public function store(CreateRequest $request, ImageService $imageService, SlugService $slugService)
    {
        $data = $request->all();
//        dd($data);
        $data += ['slug' => $slugService->makeSlug($data['title'], 'products')];
        $data += ['image_1' => $imageService->saveImage($request->file('image_1_file'), 'products', 400, 615)];
        $data += ['image_2' => $imageService->saveImage($request->file('image_2_file'), 'products', 400, 615)];
        $data += ['image_3' => $imageService->saveImage($request->file('image_3_file'), 'products', 400, 615)];
        $product = Product::create($data);
//        dd($data);

        return redirect('/admin/products')->with('flash_message', "Товар {$product->title} добавлен!");
    }

    public function edit(Product $product)
    {
        $designers = Designer::all();
        $categories = Category::main()->orderBy('title')->get();
        return view('Admin.products.edit', compact('product', 'designers', 'categories'));
    }

    public function update(UpdateRequest $request, ImageService $imageService, Product $product)
    {
        $data = $request->all();
//        dd($data);

        if(empty($data['show_recommendations'])) $data['show_recommendations'] = false;
        if(empty($data['sizes'])) $data['sizes'] = null;
//        dd($data);

        if (!empty($request->file('image_1_file')))
        {
            if (!empty($product->image_1)) Storage::delete('/public/uploaded_images/products/' . $product->image_1);
            $data += ['image_1' => $imageService->saveImage($request->file('image_1_file'), 'products', 400, 615)];
        }

        if (!empty($request->file('image_2_file')))
        {
            if (!empty($product->image_2)) Storage::delete('/public/uploaded_images/products/' . $product->image_2);
            $data += ['image_2' => $imageService->saveImage($request->file('image_2_file'), 'products', 400, 615)];
        }

        if (!empty($request->file('image_3_file')))
        {
            if (!empty($product->image_3)) Storage::delete('/public/uploaded_images/products/' . $product->image_3);
            $data += ['image_3' => $imageService->saveImage($request->file('image_3_file'), 'products', 400, 615)];
        }

        $old_name = $product->title;
        $product->update($data);

        return redirect('/admin/products')->with('flash_message', "Товар {$old_name} изменён!");
    }

    public function delete(Product $product)
    {
        /*if (!empty($product->image_1)) Storage::delete('/public/uploaded_images/products/' . $product->image_1);
        if (!empty($product->image_2)) Storage::delete('/public/uploaded_images/products/' . $product->image_2);
        if (!empty($product->image_3)) Storage::delete('/public/uploaded_images/products/' . $product->image_3);*/

        $product->delete();
        return redirect('/admin/products')->with('flash_message', 'Товар - "' . $product->title . '" удалён!');
    }
}
