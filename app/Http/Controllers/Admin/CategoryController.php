<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Services\SlugService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function create()
    {
//        $mainCategories = Category::where('parent_id', 0)->orderBy('title')->get();
        $mainCategories = Category::main()->orderBy('title')->get();
        return view('Admin.categories.create', compact('mainCategories'));
    }

    public function store(Request $request, SlugService $slugService)
    {
        $data = $request->all();
        $data += ['slug' => $slugService->makeSlug($data['title'], 'categories')];

        $category = Category::create($data);
        return redirect('/admin/categories')->with('flash_message', "Категория {$category->title} добавлена!");
    }

    public function index()
    {
//        $mainCategories = Category::where('parent_id', 0)->orderBy('title')->get();
        $mainCategories = Category::main()->orderBy('title')->get();

        return view('Admin.categories.index', compact('mainCategories'));
    }

    public function edit(Category $category)
    {
        $mainCategories = Category::main()->where('id', '!=', $category->id)->orderBy('title')->get();
        return view('Admin.categories.edit', compact('category', 'mainCategories'));
    }

    public function update(Request $request, Category $category)
    {
        $data = $request->all();

        $old_name = $category->title;
        $category->update($data);

        return redirect('/admin/categories')->with('flash_message', "Категория {$old_name} обновлена!");
    }

    public function delete(Category $category)
    {
        $category->delete();
        return redirect('/admin/categories')->with('flash_message', 'Категория - "' . $category->title . '" удалена!');
    }
}
