<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Articles\CreateRequest;
use App\Http\Requests\Admin\Articles\UpdateRequest;
use App\Models\Article;
use App\Services\ImageService;
use App\Services\SlugService;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy('date', 'desc')->get();
        return view('Admin.articles.index', compact('articles'));
    }

    public function create()
    {
        return view('Admin.articles.create');
    }

    public function store(CreateRequest $request, ImageService $imageService, SlugService $slugService)
    {
        $data = $request->all();
//        dd($data);

        $data += ['slug' => $slugService->makeSlug($data['title'], 'articles')];
        $data += ['image_for_desktop' => $imageService->saveImage($request->file('desktop_image_file'), 'articles', 1920, 830)];
        $data += ['image_for_desktop_small' => $imageService->saveImage($request->file('desktop_image_file'), 'articles/small', 505, 350)];
        $data += ['image_for_mobile' => $imageService->saveImage($request->file('mobile_image_file'), 'articles/mobile', 250, 120)];

        $article = Article::create($data);

        return redirect('/admin/articles')->with('flash_message', "Новость {$article->title} добавлена!");
    }

    public function edit(Article $article)
    {
        return view('Admin.articles.edit', compact('article'));
    }

    public function update(UpdateRequest $request, ImageService $imageService, Article $article)
    {
        $data = $request->all();

        if (!empty($request->file('desktop_image_file')))
        {
            if (!empty($article->image_for_desktop)) Storage::delete('/public/uploaded_images/articles/' . $article->image_for_desktop);
            if (!empty($article->image_for_desktop_small)) Storage::delete('/public/uploaded_images/articles/small/' . $article->image_for_desktop_small);

            $data += ['image_for_desktop' => $imageService->saveImage($request->file('desktop_image_file'), 'articles', 1920, 830)];
            $data += ['image_for_desktop_small' => $imageService->saveImage($request->file('desktop_image_file'), 'articles/small', 505, 350)];
        }

        if (!empty($request->file('mobile_image_file')))
        {
            if (!empty($article->image_for_mobile)) Storage::delete('/public/uploaded_images/articles/mobile/' . $article->image_for_mobile);
            $data += ['image_for_mobile' => $imageService->saveImage($request->file('mobile_image_file'), 'articles/mobile', 250, 120)];
        }

        $old_name = $article->title;
        $article->update($data);

        return redirect('/admin/articles')->with('flash_message', "Новость {$old_name} обновлена!");
    }

    public function delete(Article $article)
    {
        if (!empty($article->image_for_desktop)) Storage::delete('/public/uploaded_images/articles/' . $article->image_for_desktop);
        if (!empty($article->image_for_desktop_small)) Storage::delete('/public/uploaded_images/articles/small/' . $article->image_for_desktop_small);
        if (!empty($article->image_for_mobile)) Storage::delete('/public/uploaded_images/articles/mobile/' . $article->image_for_mobile);

        $article->delete();
        return redirect('/admin/articles')->with('flash_message', 'Новость - "' . $article->title . '" удалена!');
    }
}
