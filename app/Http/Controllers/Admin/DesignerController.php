<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Designers\CreateRequest;
use App\Http\Requests\Admin\Designers\UpdateRequest;
use App\Models\Designer;
use App\Services\SlugService;
use Illuminate\Http\Request;

class DesignerController extends Controller
{
    public function index()
    {
        $designers = Designer::all();
        return view('Admin.designers.index', compact('designers'));
    }

    public function create()
    {
        return view('Admin.designers.create');
    }

    public function store(CreateRequest $request, SlugService $slugService)
    {
        $data = $request->all();
        $data += ['slug' => $slugService->makeSlug($data['title'], 'designers')];

        $designer = Designer::create($data);
        return redirect('/admin/designers')->with('flash_message', "bpfqyth {$designer->title} добавлен!");
    }

    public function edit(Designer $designer)
    {
        return view('Admin.designers.edit', compact('designer'));
    }

    public function update(UpdateRequest $request, Designer $designer)
    {
        $data = $request->all();

        $old_name = $designer->title;
        $designer->update($data);

        return redirect('/admin/designers')->with('flash_message', "Дизайнер {$old_name} обновлён!");
    }

    public function delete(Designer $designer)
    {
        $designer->delete();
        return redirect('/admin/designers')->with('flash_message', "Дизайнер {$designer->title} удалён!");
    }
}
