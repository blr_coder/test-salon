<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function show($slug)
    {
        $product = Product::where('slug', $slug)->first();
        return view('catalog.show', compact('product'))
            ->with('page_title', $product->title . ' в салоне вечерней моды - Sensation')
            ->with('page_description', $product->description);
    }
}
