<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;


class CatalogController extends Controller
{
    public function index($category = null, $sub_category = null)
    {
        if (empty($category))
        {
            return view('catalog.index')
                ->with('page_title', 'Каталог салона вечерней моды - Sensation')
                ->with('page_description', 'Salon Sensation - салон вечерних и свадебных платьев в Минске. Продажа вечерних, выпускных и коктейльных платьев.');
        }

        elseif (!empty($category) && empty($sub_category))
        {
//            @dd($category);
            $categoryNow = Category::where('slug', $category)->first();
            $categoryId = $categoryNow->id;
            return view('catalog.index', compact('categoryId'));
        }

        elseif (!empty($category) && !empty($sub_category))
        {
//            @dd($category, $sub_category);
            $categoryNow = Category::where('slug', $category)->first();
            $categoryId = $categoryNow->id;

            $subCategory = Category::where('slug', $sub_category)->first();
            $subCategoryId = $subCategory->id;
            return view('catalog.index', compact('categoryId', 'subCategoryId'));
        }
    }

    public function static()
    {
        $staticLink = '';
        return view('catalog.index');
    }

}

