<?php

namespace App\Http\Requests\Admin\Photos;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:photos|string|max:190',
            'images' => 'required|array',
            'images.*' => 'required|image|max:1024',
        ];
    }
}
