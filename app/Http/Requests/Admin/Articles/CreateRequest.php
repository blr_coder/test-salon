<?php

namespace App\Http\Requests\Admin\Articles;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }



    public function rules():array
    {
        return [
            'title' => 'required|unique:articles|string|max:190',
            'preview' => 'required|max:500',
            'description' => 'required|max:20000',
            'date' => 'nullable|date',
            'desktop_image_file' => 'required|image|max:5048',
            'mobile_image_file' => 'required|image|max:5048',
        ];
    }
}
