<?php

namespace App\Http\Requests\Admin\Products;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string|max:190',
            'model' => 'required|string|max:190',
            'price' => 'required|integer',
            'discount_price' => 'nullable|max:190',

            'image_1_file' => 'image|max:2048',
            'image_2_file' => 'image|max:2048',
            'image_3_file' => 'image|max:2048',

            'video' => 'nullable|max:190',

            'description' => 'nullable|max:20000',
        ];
    }
}
