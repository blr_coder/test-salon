<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title');
            $table->string('slug');
            $table->string('model');
            $table->text('description')->nullable();

            $table->string('price');
            $table->string('discount_price')->nullable();

            $table->string('image_1')->nullable();
            $table->string('image_2')->nullable();
            $table->string('image_3')->nullable();

            $table->text('video')->nullable();

            $table->integer('status')->nullable();

            $table->smallInteger('new_or_special_price')->nullable();   //1 - new, 2 - special_price, 0 - null

            $table->json('sizes')->nullable();

            $table->string('colors')->nullable();

            $table->bigInteger('category_id');
            $table->bigInteger('designer_id');

            $table->boolean('show_recommendations')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
