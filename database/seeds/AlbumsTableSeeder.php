<?php

use Illuminate\Database\Seeder;

class AlbumsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('albums')->delete();
        
        \DB::table('albums')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Свадебный фестиваль BRIDE’S DAY',
                'slug' => 'svadebnyy-festival-brides-day',
                'date' => NULL,
                'image_for_desktop' => 'TVRoVid1gKRC5i05H5XD1qTMZFuUkshF.jpg',
                'image_for_desktop_small' => 'RObEhC5VTdzxo9iTKN8WWAthgmpUCF6l.jpg',
                'image_for_mobile' => 'iQDceluf2s5DDPCCqZGW1i0i5RKYWAfT.jpg',
                'photographer' => 'XXX',
                'location' => 'Минск',
                'created_at' => '2020-02-24 16:53:05',
                'updated_at' => '2020-02-24 16:53:05',
            ),
        ));
        
        
    }
}