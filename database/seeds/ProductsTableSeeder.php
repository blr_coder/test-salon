<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 4,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-50632',
                'model' => '93120',
                'description' => '<p>&nbsp;Под заказ возможны любые размеры, а также цвета:</p>
<ul>
<li>красный</li>
<li>черный</li>
<li>изумрудный</li>
<li>бордо</li>
</ul>',
                'price' => '1760',
                'discount_price' => NULL,
                'image_1' => '0qLC5RbvSsJqVPiqK4ARthGZhmp3ce2X.jpg',
                'image_2' => 'QFZQU2pAsVllXWheJT3XjLCpsPUygHCh.jpg',
                'image_3' => 'Rj62reJxBiOuYz7lFnSdZOUx0hbS8Wne.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"23": "38"}',
            'colors' => 'Navy (темно-синий)',
                'category_id' => 6,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-03 16:29:45',
                'updated_at' => '2020-03-04 14:16:23',
            ),
            1 => 
            array (
                'id' => 5,
                'title' => 'Платье Sherri Hill',
                'slug' => 'plate-sherri-hill-53148',
                'model' => '53148',
                'description' => '<p>Под заказ возможны любые размеры, а так же цвета:&nbsp;</p>
<ul>
<li>черный</li>
<li>голубой</li>
<li>красный</li>
</ul>
<p>&nbsp;</p>',
                'price' => '1550',
                'discount_price' => NULL,
                'image_1' => 'omm6lKPnIloFHlKnlFZ0FcfHggobv4ye.jpg',
                'image_2' => 'SPWcgXmFkjNr3xINESzzKyuQh1HPS4Xt.jpg',
                'image_3' => 'BuZvdc9lNKRTACdTtEAjyAZprtwxARio.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"5": "8"}',
            'colors' => 'Silver (серебро)',
                'category_id' => 2,
                'designer_id' => 2,
                'show_recommendations' => 1,
                'created_at' => '2020-03-03 17:04:38',
                'updated_at' => '2020-03-04 14:10:33',
            ),
            2 => 
            array (
                'id' => 6,
                'title' => 'Платье Terani Couture',
                'slug' => 'plate-terani-couture-1622e1578',
                'model' => '1578',
                'description' => '<p>Под заказ возможны любые размеры, а также цвета:</p>
<p>&nbsp; &nbsp; rose</p>
<p>&nbsp;&nbsp;</p>',
                'price' => '2700',
                'discount_price' => NULL,
                'image_1' => 'LTaUbZPVsXsNpbqPn1ToPwOTid43i93X.jpg',
                'image_2' => 'pPohWwg7SZoPbUz8KHTh7tog3RpVJBlY.jpg',
                'image_3' => '1sjOtfCCu5teT8v3ytnEGxSO6PXSmj8N.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"4": "6"}',
            'colors' => 'MOCHA (мокка)',
                'category_id' => 6,
                'designer_id' => 3,
                'show_recommendations' => 1,
                'created_at' => '2020-03-03 23:21:21',
                'updated_at' => '2020-03-04 14:20:02',
            ),
            3 => 
            array (
                'id' => 7,
                'title' => 'Платье Terani Couture',
                'slug' => 'plate-terani-couture-c3678',
                'model' => '3678',
                'description' => '<p>Плотное трикотажное платье с баской, длина чуть выше колена. Силуэт - футляр, очень красиво вытягивающий фигуру</p>',
                'price' => '1400',
                'discount_price' => '700',
                'image_1' => 'DF4GMIImWpLD64gd4HZMukL00zhBaSbv.jpg',
                'image_2' => 'VZdM4NGF7yClAQdxYUXssg8rzf4LClb2.jpg',
                'image_3' => 'iHQEEYZx47LD50YAsyVvvvuceR4TleWz.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 2,
                'sizes' => '{"3": "4", "4": "6"}',
                'colors' => 'black, red',
                'category_id' => 2,
                'designer_id' => 3,
                'show_recommendations' => 1,
                'created_at' => '2020-03-03 23:32:06',
                'updated_at' => '2020-03-04 14:23:07',
            ),
            4 => 
            array (
                'id' => 8,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93918',
                'model' => '93918',
                'description' => '<p style="text-align: right;">Нежное, воздушное платье А-силуэта с завязками на плечах. Под заказ возможны любые размеры&nbsp; &nbsp;</p>',
                'price' => '2440',
                'discount_price' => NULL,
                'image_1' => '9TAT6biwXXQoNWKvftsFhsr1KdiNWGvE.jpg',
                'image_2' => 'nxlclALyvCZxy086au4ivExno6MweheM.jpg',
                'image_3' => 'c6yr5FOjlzCZlcP7fItQEh2tjKQPHNmB.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"22": "36"}',
                'colors' => 'grey-silver',
                'category_id' => 6,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-03 23:38:50',
                'updated_at' => '2020-03-04 14:41:04',
            ),
            5 => 
            array (
                'id' => 9,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-50760',
                'model' => '50760',
                'description' => '<ul>
<li style="text-align: right;"></li>
<li style="text-align: right;">&nbsp;</li>
</ul>',
                'price' => '1870',
                'discount_price' => NULL,
                'image_1' => 'HQG0mlCulJJg7JUWehojm3Fzkw9Qdzqc.jpg',
                'image_2' => 'oFBI455ESLuOMZnsBxVPGX8YXK1syeL4.jpg',
                'image_3' => 'shraE6yVcKgB1uftnGUXlFm5RQVbXqHO.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"24": "40"}',
                'colors' => 'blue',
                'category_id' => 6,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-03 23:46:25',
                'updated_at' => '2020-03-04 14:17:09',
            ),
            6 => 
            array (
                'id' => 10,
                'title' => 'Платье Sherri Hill',
                'slug' => 'plate-sherri-hill-53060',
                'model' => '53060',
                'description' => '<p>Под&nbsp;</p>',
                'price' => '1990',
                'discount_price' => NULL,
                'image_1' => 'BxzfxGV0uDcxm227YT8DkQsPb4SCe2yp.jpg',
                'image_2' => 'Et4X2zwz5nvhSasSS60UKX1EvmzNLp6E.jpg',
                'image_3' => 'UXpyuHqbvcZ423hPatorXqZKbRRrVuGD.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"3": "4"}',
                'colors' => 'nude/silver',
                'category_id' => 2,
                'designer_id' => 2,
                'show_recommendations' => 1,
                'created_at' => '2020-03-03 23:57:56',
                'updated_at' => '2020-03-04 14:41:58',
            ),
            7 => 
            array (
                'id' => 11,
                'title' => 'Платье Sherri Hill',
                'slug' => 'plate-sherri-hill-52091',
                'model' => '52091',
                'description' => '<p>под</p>',
                'price' => '2400',
                'discount_price' => NULL,
                'image_1' => 'PCsNhPZkspwq5Fs0jLVDR5awuOQpAy6p.jpg',
                'image_2' => '5rFhKE8loiNOqPfGHBHIbUSmXs6ALwNy.jpg',
                'image_3' => 'vBJLeEqpIUrzJSCDlQf7JAWJMKxj8H1W.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"4": "6"}',
                'colors' => 'black',
                'category_id' => 2,
                'designer_id' => 2,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 00:02:13',
                'updated_at' => '2020-03-04 14:42:44',
            ),
            8 => 
            array (
                'id' => 12,
                'title' => 'Платье Sherri Hill',
                'slug' => 'plate-sherri-hill-52127',
                'model' => '52127',
                'description' => '<p>под</p>',
                'price' => '1950',
                'discount_price' => NULL,
                'image_1' => 'GdqJ6zHp39Fo6fa3ykYKO1Id50kkgtcZ.jpg',
                'image_2' => 'ayQmLaGqFNFIW15at8AkFoC6SJys5UR5.jpg',
                'image_3' => 'fUkqgUygZgIkBxWGuaHrRpuhx68LQwMW.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"1": "0"}',
                'colors' => 'black/multi',
                'category_id' => 2,
                'designer_id' => 2,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 00:06:25',
                'updated_at' => '2020-03-04 14:43:00',
            ),
            9 => 
            array (
                'id' => 13,
                'title' => 'Платье Sherri Hill',
                'slug' => 'plate-sherri-hill-52157',
                'model' => '52157',
                'description' => '<p>под</p>',
                'price' => '2250',
                'discount_price' => NULL,
                'image_1' => 'XASoeCGTn8Qr0zGk98HeINH4UEucUfsR.jpg',
                'image_2' => 'wJ7d9LTe2DzUzF5yombfUDHIjiNad1dY.jpg',
                'image_3' => 'IE3KR6W2jabJdp7Pqo8iJhwe4BNc5sUf.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"4": "6", "5": "8"}',
                'colors' => 'blush, ivory/nude',
                'category_id' => 2,
                'designer_id' => 2,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 00:10:00',
                'updated_at' => '2020-03-04 14:42:27',
            ),
            10 => 
            array (
                'id' => 14,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93897',
                'model' => '93897',
                'description' => '<p>под</p>',
                'price' => '1900',
                'discount_price' => NULL,
                'image_1' => 'I09rC2dTG2PPItBAi7Rttlzut8fcQUNC.jpg',
                'image_2' => '1Le2TaCWSB6ulmi9xGeFiXHPot8Ksakj.jpg',
                'image_3' => '7PGTcCiyqbPyRXRNBBqJEgGbkT0x4wRN.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"23": "38"}',
                'colors' => 'ivory',
                'category_id' => 7,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 00:13:22',
                'updated_at' => '2020-03-04 14:43:44',
            ),
            11 => 
            array (
                'id' => 15,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93637',
                'model' => '93637',
                'description' => '<p>под</p>',
                'price' => '1900',
                'discount_price' => NULL,
                'image_1' => 'vOeBvGz8sjeGn3zcCcIvLmSnHTj1W2zp.jpg',
                'image_2' => 'iCOXd0715ZEd9b4BxZnRtAHjvMTLtVTR.jpg',
                'image_3' => 'LPesAHVX19lpLv51hEXuYdsM436UusAP.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"24": "40"}',
                'colors' => 'smoke blue',
                'category_id' => 1,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 00:18:09',
                'updated_at' => '2020-03-04 14:46:05',
            ),
            12 => 
            array (
                'id' => 16,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-50620',
                'model' => '50620',
                'description' => '<p>под</p>',
                'price' => '2000',
                'discount_price' => NULL,
                'image_1' => 'Hyeu4fY7XnigihBdqIib5rXHIftc0Iaq.jpg',
                'image_2' => 'glvyDWXhesDo1lky4CqZUUt0d4TkJdcV.jpg',
                'image_3' => 'SlTfln9TvGAzYPo2AnpbYp4pQPmwADr7.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"24": "40"}',
                'colors' => 'red',
                'category_id' => 1,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 00:22:17',
                'updated_at' => '2020-03-04 14:43:22',
            ),
            13 => 
            array (
                'id' => 17,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93710',
                'model' => '93710',
                'description' => '<p>под</p>',
                'price' => '1890',
                'discount_price' => NULL,
                'image_1' => 'nK3spknBMEYFWuRWGspIY3jxhh2hEVgh.jpg',
                'image_2' => 'AvE27xqQMwhZer1geibBL9p5khiqmdBQ.jpg',
                'image_3' => 'ucmy1sKcPHjivJixv8U8xMEs0r9ourRc.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"23": "38"}',
                'colors' => 'navy',
                'category_id' => 6,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 00:25:00',
                'updated_at' => '2020-03-04 14:44:03',
            ),
            14 => 
            array (
                'id' => 18,
                'title' => 'Комбинезон Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93921',
                'model' => '93921',
                'description' => '<p>под</p>',
                'price' => '1830',
                'discount_price' => NULL,
                'image_1' => 'HIQv038TYvzxtqvqUyOLphEDFZpfjc9d.jpg',
                'image_2' => 'p8gtSWVZUoNilENkpfTm13eorYRlvxUr.jpg',
                'image_3' => 'xChyQ2SnfMgLIgLHFyWBKYHOxr6laB6n.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"14": "S"}',
                'colors' => 'navy',
                'category_id' => 6,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 00:27:25',
                'updated_at' => '2020-03-04 14:11:21',
            ),
            15 => 
            array (
                'id' => 19,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93832',
                'model' => '93832',
                'description' => '<p>под</p>',
                'price' => '1600',
                'discount_price' => NULL,
                'image_1' => 'M75O4HmTYapdLFRJvYKNp6KPW99DrrLV.jpg',
                'image_2' => 'Qb73fOJp9QoI6BejBfmzl2XdVs9oBAk9.jpg',
                'image_3' => 'vPCPHHEneEVU9kLpIzkBIIJiSGk75PJm.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"22": "36"}',
                'colors' => 'powder',
                'category_id' => 2,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 11:46:34',
                'updated_at' => '2020-03-04 14:44:45',
            ),
            16 => 
            array (
                'id' => 20,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93955',
                'model' => '93955',
                'description' => '<p>под</p>',
                'price' => '1580',
                'discount_price' => NULL,
                'image_1' => 'qw2pqvaZ8YIk2gkceslIBDrtS5eNdMA1.jpg',
                'image_2' => 'LBnprPJyH5pFXaKbuaG6Fd9xkgGgU3wn.jpg',
                'image_3' => '988CgdhvaKgvj7AjG04Uq9sXY2o1e8wc.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"22": "36"}',
                'colors' => 'navy',
                'category_id' => 2,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 11:48:48',
                'updated_at' => '2020-03-04 14:38:54',
            ),
            17 => 
            array (
                'id' => 22,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93862',
                'model' => '93862',
                'description' => '<p>под</p>',
                'price' => '1830',
                'discount_price' => NULL,
                'image_1' => 'OLqiPcYXcV9uFHQUdQbvKrR9BP8qlNb4.jpg',
                'image_2' => '3EAGUSg7Pr6bObTXgOEeuMshcVzQpnq4.jpg',
                'image_3' => 'm7qyVFSjQwJ8vJ0iXYwVBVNNxGv2OqlB.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"26": "44"}',
                'colors' => 'black/gold',
                'category_id' => 2,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 11:54:05',
                'updated_at' => '2020-03-04 14:46:29',
            ),
            18 => 
            array (
                'id' => 23,
                'title' => 'Платье Sherri Hill',
                'slug' => 'plate-sherri-hill-53292',
                'model' => '53292',
                'description' => '<p>Вечернее платье с длинными рукавами и V-образным вырезом. Спинка открытая на шнуровке. Вышивка бусинами и камнями на легкой кружевной, сетчатой основе. Под заказ возможны любые размеры,&nbsp;уточнить их наличие можно у менеджера бутика по контактному номеру телефона.</p>',
                'price' => '3180',
                'discount_price' => NULL,
                'image_1' => 'lwDWtqyP3tabxwF0gFjntqrxeVamPrrz.jpg',
                'image_2' => '8Mc5p7SmzkMKs1cx3CWQVnyr4NitGkxe.jpg',
                'image_3' => 'Gb4ZNVwujFJpw6iLcxpbNfwWWGC6DFPX.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => NULL,
                'sizes' => '{"4": "6"}',
                'colors' => 'nude/silver',
                'category_id' => 1,
                'designer_id' => 2,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 12:12:02',
                'updated_at' => '2020-03-04 14:46:45',
            ),
            19 => 
            array (
                'id' => 24,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93904',
                'model' => '93904',
                'description' => '<p>Платье с рукавами и акцентными плечами. Длина немного регулируется за счет складок на бедрах, мягкое,комфортное и не мнущееся.&nbsp;</p>',
                'price' => '1830',
                'discount_price' => NULL,
                'image_1' => 'BMuqd1R1kUVR1AeKC2I4i69tW8rkedmW.jpg',
                'image_2' => 'GBdZHdhGsSSIWx9m8yD2otI1R7trItdf.jpg',
                'image_3' => '4DWx0kPLjMmbqPIrzsoweGL28uBtEftv.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"22": "36"}',
                'colors' => 'gold',
                'category_id' => 5,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 12:13:49',
                'updated_at' => '2020-03-04 14:49:33',
            ),
            20 => 
            array (
                'id' => 25,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93336',
                'model' => '93336',
                'description' => '<p>под</p>',
                'price' => '1840',
                'discount_price' => NULL,
                'image_1' => 'vxIJQff8Orlf8AHudSFvWjSJ0Ayo3kzK.jpg',
                'image_2' => 'd589D8QvWrEEO7d7ExhkZZoW9lvQASUL.jpg',
                'image_3' => '02ixF1jVgQf2KNZmdtrsFZMI3hyqOFVI.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"24": "40"}',
                'colors' => 'wine',
                'category_id' => 2,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 12:15:54',
                'updated_at' => '2020-03-04 14:45:01',
            ),
            21 => 
            array (
                'id' => 26,
                'title' => 'Tarik Ediz 93886',
                'slug' => 'tarik-ediz-93886',
                'model' => '93886',
                'description' => '<p>под</p>',
                'price' => '1590',
                'discount_price' => NULL,
                'image_1' => 'CuiN74VLt1WRWnOSwmzIg5tRucKpNScI.jpg',
                'image_2' => 'D8HHBCpm1eqt7VXx16UyfBRCkjnDsBJ2.jpg',
                'image_3' => 'ZnL61kan3qOMFiuBEy1Ntl1T71t9Oh4t.JPG',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"22": "36"}',
                'colors' => 'black/gold',
                'category_id' => 2,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 12:20:23',
                'updated_at' => '2020-03-04 13:19:21',
            ),
            22 => 
            array (
                'id' => 27,
                'title' => 'Платье Tarik Ediz',
                'slug' => 'plate-tarik-ediz-93870',
                'model' => '92111',
                'description' => '<p>под</p>',
                'price' => '1590',
                'discount_price' => NULL,
                'image_1' => 'GQcVfhuaiWFQTceSl5DkDTRe5il8rjGf.jpg',
                'image_2' => '3okBBWvhpsQ7UMLJjRAvoZ6QEnrQsVM4.jpg',
                'image_3' => 'veZu5mWRqjtvHFp7GIJGYLyYjJFwsc4p.jpg',
                'video' => NULL,
                'status' => 1,
                'new_or_special_price' => 1,
                'sizes' => '{"24": "40"}',
                'colors' => 'petrol',
                'category_id' => 2,
                'designer_id' => 1,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 12:22:22',
                'updated_at' => '2020-03-04 14:44:30',
            ),
            23 => 
            array (
                'id' => 28,
                'title' => 'Платье',
                'slug' => 'plate',
                'model' => 'Fouad Sarkis',
                'description' => '<p>под</p>',
                'price' => '1600',
                'discount_price' => '900',
                'image_1' => 'mWBEZ8YdPMmZROMLSO8vCTbomDf5sAX1.JPG',
                'image_2' => 'aD9k0pDsAbcyfv1BHnZgX2O4sKlixlom.JPG',
                'image_3' => '80d9a29REKGfCMAzoThaaair6z0KFTr9.JPG',
                'video' => 'https://youtu.be/DRi7vS7pZj0',
                'status' => 1,
                'new_or_special_price' => 2,
                'sizes' => '{"16": "L"}',
                'colors' => 'gold',
                'category_id' => 1,
                'designer_id' => 6,
                'show_recommendations' => 1,
                'created_at' => '2020-03-04 13:26:57',
                'updated_at' => '2020-03-04 13:26:57',
            ),
        ));
        
        
    }
}