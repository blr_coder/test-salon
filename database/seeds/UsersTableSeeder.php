<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Developer',
                'email' => '05011989e@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$SgYRjSlSBOAPc9p.leixHOv8gdSr8/YY9hdSjv7yyy8rd4YXx8NKa',
                'role' => 'superAdmin',
                'remember_token' => NULL,
                'created_at' => '2020-02-24 14:52:49',
                'updated_at' => '2020-02-24 14:52:49',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Aleks',
                'email' => 'sashawolchok@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$HD3Ocr3SNJ35hBkhSq3Ar.FZtbETpyq5VQEA1Ex9QzqKLgbbtHxbC',
                'role' => 'Admin',
                'remember_token' => NULL,
                'created_at' => '2020-02-24 15:04:34',
                'updated_at' => '2020-02-24 15:04:34',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Admin',
                'email' => 'Admin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$DYmVn.HHBnu9.epB1jPnP.W4b6CoTDuaxTXsKN1ZFRl6lp26HOKTq',
                'role' => 'Admin',
                'remember_token' => 'bRJfXficRLI7zBnXry2M6B2YSc2Y0MQFVn57aURoikBVKwMQz4bgLULq89d4',
                'created_at' => '2020-02-24 17:05:29',
                'updated_at' => '2020-02-24 17:05:29',
            ),
        ));
        
        
    }
}