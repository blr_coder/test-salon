<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Вечерние платья',
                'slug' => 'vechernie-platya',
                'parent_id' => 0,
                'number' => 0,
                'created_at' => '2020-02-24 14:55:14',
                'updated_at' => '2020-02-24 14:55:14',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Коктейльные платья',
                'slug' => 'kokteylnye-platya',
                'parent_id' => 0,
                'number' => 0,
                'created_at' => '2020-02-24 14:55:40',
                'updated_at' => '2020-02-24 14:55:40',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Свадебные платья',
                'slug' => 'svadebnye-platya',
                'parent_id' => 0,
                'number' => 0,
                'created_at' => '2020-02-25 16:17:05',
                'updated_at' => '2020-02-25 16:17:05',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Платья на выпускной',
                'slug' => 'platya-na-vypusknoy',
                'parent_id' => 0,
                'number' => 0,
                'created_at' => '2020-02-25 16:17:13',
                'updated_at' => '2020-02-25 16:17:13',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Аксессуары',
                'slug' => 'aksessuary',
                'parent_id' => 0,
                'number' => 0,
                'created_at' => '2020-02-25 16:17:27',
                'updated_at' => '2020-02-25 16:17:27',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Вечерние',
                'slug' => 'vechernie',
                'parent_id' => 1,
                'number' => 0,
                'created_at' => '2020-02-25 16:17:45',
                'updated_at' => '2020-02-25 16:35:16',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Свадебно-вечерние',
                'slug' => 'svadebno-vechernie',
                'parent_id' => 1,
                'number' => 0,
                'created_at' => '2020-02-25 16:17:55',
                'updated_at' => '2020-02-25 16:17:55',
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Очки',
                'slug' => 'ochki',
                'parent_id' => 5,
                'number' => 0,
                'created_at' => '2020-03-03 16:05:01',
                'updated_at' => '2020-03-03 16:05:01',
            ),
            8 => 
            array (
                'id' => 9,
                'title' => 'Под заказ',
                'slug' => 'pod-zakaz',
                'parent_id' => 3,
                'number' => 0,
                'created_at' => '2020-03-03 16:16:11',
                'updated_at' => '2020-03-03 16:16:11',
            ),
        ));
        
        
    }
}