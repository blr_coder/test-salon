<?php

use Illuminate\Database\Seeder;

class DesignersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('designers')->delete();
        
        \DB::table('designers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'TARIK EDIZ',
                'slug' => 'tarik-ediz',
                'created_at' => '2020-02-24 14:56:28',
                'updated_at' => '2020-02-24 14:56:28',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'SHERRI HILL',
                'slug' => 'sherri-hill',
                'created_at' => '2020-03-03 16:06:28',
                'updated_at' => '2020-03-03 16:06:28',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Terani Couture',
                'slug' => 'terani-couture',
                'created_at' => '2020-03-03 16:07:03',
                'updated_at' => '2020-03-03 16:07:03',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'NICOLE BAKTI',
                'slug' => 'nicole-bakti',
                'created_at' => '2020-03-03 16:07:46',
                'updated_at' => '2020-03-03 16:08:13',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'SIVALIA COUTURE',
                'slug' => 'sivalia-couture',
                'created_at' => '2020-03-03 16:09:11',
                'updated_at' => '2020-03-03 16:09:11',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'FOUAD SARKIS',
                'slug' => 'fouad-sarkis',
                'created_at' => '2020-03-03 16:10:30',
                'updated_at' => '2020-03-03 16:10:30',
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'JOVANI',
                'slug' => 'jovani',
                'created_at' => '2020-03-03 16:11:21',
                'updated_at' => '2020-03-03 16:11:21',
            ),
        ));
        
        
    }
}