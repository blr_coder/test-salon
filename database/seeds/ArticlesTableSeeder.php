<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('articles')->delete();
        
        \DB::table('articles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'СВАДЬБА И ПЛАТЬЕ В СТИЛЕ  КАНТРИ',
                'preview' => 'Сейчас популярными стали свадьбы на природе, с интересной стилизацией и историей, с совершенно необычными фото, особенно если это не модная усадьба, а самобытная деревня.',
                'description' => '<p>Сейчас популярными стали свадьбы на природе, с интересной стилизацией и историей, с совершенно необычными фото, особенно если это не модная усадьба, а самобытная деревня. <br />Никаких ограничений: декоратор воплотит и поддержит любую идею, организатор возьмёт на себя все заботы..<br />Но! как не выбиться из общего стиля, если и невестой хочется побыть и свадебные кринолины в такой обстановке неуместны?<br />Оцените! NEW! Топ с юбкой от турецкого бренда Тарик Эдиз полностью вписывается в концепт деревни, не превратит вас в самобытную селянку, а лишь отыграет нужный контекст события.<br />Натуральное кружево, восхитительный крой &mdash; это все богемный кантри, в котором невеста просто искупается в комплиментах. Лёгкий локон, шляпа и удобные сапожки - и этот свадебный день запомнится навсегда!<br />Все самые необычные свадебные наряды с удовольствием подберем для вас в стенах нашего салона.</p>
<p><img class="" src="http://salon-sensation.ilavista.com/source/IMG_4783.jpg" alt="" width="300" height="450" /><img class="" src="http://salon-sensation.ilavista.com/source/IMG_4784.jpg" alt="" width="300" height="450" /><img class="" src="http://salon-sensation.ilavista.com/source/IMG_4785.jpg" alt="" width="300" height="450" /><img class="" src="http://salon-sensation.ilavista.com/source/IMG_4786.jpg" alt="" width="300" height="450" /></p>',
                'date' => '2020-02-24',
                'image_for_desktop' => 'oDTAzo1xkJr7j0ZLAFDJA8M0TrxshSiH.JPG',
                'image_for_desktop_small' => 'LAqJylesrfXK2tVMVSyJj60zQixiy4eA.JPG',
                'image_for_mobile' => 'erlXuo9V996URZbQ3vEXT5Q5Letg88Il.JPG',
                'slug' => 'svadba-i-plate-v-stile-kantri',
                'created_at' => '2020-02-24 15:01:06',
                'updated_at' => '2020-02-24 15:01:06',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Ночь классики в метро!',
                'preview' => 'Ночь классики в метро! В рамках фестиваля Юрия Башмета. наш показ прошёл под звёздным небом в сопровождении классической музыки камерного оркестра  «East-West Chamber Orchestra» под управлением Ростислава Кримера',
                'description' => '<p>Ночь классики в метро! В рамках фестиваля Юрия Башмета. наш показ прошёл под звёздным небом в сопровождении классической музыки камерного оркестра &nbsp;&laquo;East-West Chamber Orchestra&raquo; под управлением Ростислава Кримера <br /> ⠀</p>
<p>Начало концерта было в 2 ночи, прямо на перроне установили площадку-сцену для оркестра, места для зрителей и оборудовали место для прессы. Работу метро не остановили в этот день раньше, все работало по графику, а это значит, что монтаж площадки проводился в максимально короткие сроки. На это было меньше часа!</p>
<p>Плюсы:<br /> - красиво, необычно и эффектно, звездное небо и темные вагонные составы &mdash; таким был фон для мероприятия.<br /> - В метро на удивление классная акустика, было много света, и много оттенков подсветки.<br /> - тайминг просто шикарный, все в точности до минуты, из-за того, что это достаточно сложный объект, все контролировалось дотошно.<br /> - удобное расположение для СМИ и прессы, правильные места для съемки<br /> - отдельные комнаты для артистов и раздевалки для моделей, что для нас было даже странно. Ожидали мы, что переодевать моделей нужно будет чуть ли не в переходе 😂<br /> ⠀<br /> Минус<br /> - Стулья, да да пластиковые синие стулья))) Пожалуйста, пускай это будет последний момент, когда мы их упоминаем, так как слава Богу, все их осудили, а это значит, что у организаторов не будет ни малейшего шанса оставить их на будущий год <br /> ⠀<br /> Было приятно, что все относились друг к другу толерантно, и понимали, что сложность объекта накладывает определённые сложности в организации. <br /> ⠀<br /> Благодарим всех девочек, за то, что очень ответственно подошли к показу. И НШК за эту ночную авантюру !</p>
<table style="height: 533px;" width="512">
<tbody>
<tr>
<td style="width: 248px;"><img class="" src="http://salon-sensation.ilavista.com/source/_MG_9170.jpg" alt="" width="350 " height="525" /></td>
<td style="width: 248px;"><img class="" src="http://salon-sensation.ilavista.com/source/_MG_9144.jpg" alt="" width="350" height="525" /></td>
</tr>
<tr>
<td style="width: 248px;"><img class="" src="http://salon-sensation.ilavista.com/source/_MG_9109.jpg" alt="" width="350" height="233" /></td>
<td style="width: 248px;"><img class="" src="http://salon-sensation.ilavista.com/source/_MG_8957.jpg" alt="" width="350" height="233" /></td>
</tr>
<tr>
<td style="width: 248px;">&nbsp;</td>
<td style="width: 248px;">&nbsp;</td>
</tr>
</tbody>
</table>',
            'date' => '2020-02-08',
            'image_for_desktop' => 'twtMEhvtaV3XcXWGu6MO7emHFboD7F5f.jpg',
            'image_for_desktop_small' => 'oxEWW49JSeTYDu6Y1Np4uIwFovlkPBdf.jpg',
            'image_for_mobile' => '6xKG9BwW5oZx7BH3lFrxROw2xFSBtGiO.jpg',
            'slug' => 'noch-klassiki-v-metro',
            'created_at' => '2020-02-24 15:08:55',
            'updated_at' => '2020-02-24 15:40:12',
        ),
        2 => 
        array (
            'id' => 3,
            'title' => 'Свадебный фестиваль BRIDE’S DAY',
            'preview' => '9 ФЕВРАЛЯ 2020 года на BRIDE\'S DAY мы показали коллекцию свадебных платьев бренда Tarik Ediz!',
            'description' => '<p>Свадебный фестиваль BRIDE&rsquo;S DAY&nbsp;</p>
<p>9 ФЕВРАЛЯ 2020 года на BRIDE\'S DAY мы показали коллекцию свадебных платьев бренда Tarik Ediz!</p>
<p>&nbsp;</p>
<p>Это был свадебный фестиваль для тех, кто решил пожениться! Здесь зрители&nbsp; увидели новинки 2020 года в свадебной индустрии, познакомились со свадебными специалистами, &nbsp;хорошо провели время и решили для себя, какая свадьба будет именно у них!&nbsp;</p>
<p>Специально была организована выездная церемонии с настоящей парой.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Площадка: PRIZMA Eventplace&nbsp;</p>
<p>Организаторы: Юлия Тимакова и Янина Бернацкая</p>
<p>&nbsp;</p>
<table style="height: 226px;" width="644">
<tbody>
<tr>
<td style="width: 314px;"><img class="" src="http://salon-sensation.ilavista.com/source/Brides%20day%20-0577.jpg" alt="" width="350" height="525" /></td>
<td style="width: 314px;">&nbsp;</td>
</tr>
<tr>
<td style="width: 314px;">&nbsp;</td>
<td style="width: 314px;">&nbsp;</td>
</tr>
</tbody>
</table>',
            'date' => '2020-02-09',
            'image_for_desktop' => 'AX63DM4p8Ped4iNsuRhlXrY4EYoEVkmV.jpg',
            'image_for_desktop_small' => 'lc1yIVCgcgAL4JtJYoDRjP7yecFXTBNX.jpg',
            'image_for_mobile' => 'MHYHOqQxuidOwYnwxb4WbfyhdYhOmeYe.jpg',
            'slug' => 'svadebnyy-festival-brides-day',
            'created_at' => '2020-02-24 16:38:21',
            'updated_at' => '2020-02-24 16:38:21',
        ),
    ));
        
        
    }
}