
require('./bootstrap');
require('./magnific-popup');
require('magnific-popup');
require('slick-carousel');
var Inputmask = require('inputmask');


function showPage() {
    $('#preview').css ({
        'visibility': 'hidden',
        'opacity': '0'
    });
}
setTimeout(showPage, 600);


import Vue from "vue";

Vue.component('item', require('./components/item/item').default);
Vue.component('item-card', require('./components/item/itemCard').default);
Vue.component('items-catalog', require('./components/catalog/catalog').default);
Vue.component('category-card', require('./components/catalog/categoryCard').default);
Vue.component('nav-links', require('./components/site/navLinks').default);


const app = new Vue({
    el: '#app'
});

document.addEventListener("DOMContentLoaded", () => {

    var inputMaskClass = document.getElementsByClassName("input-mask");

    var im = new Inputmask("+375 (99) 999-99-99");
    im.mask(inputMaskClass);


    //Меню при скролле!!!!!!!!!!!!!!!!!!!! + кнопка наверх
    window.onscroll = function () {

        var scrolled_e = window.pageYOffset || document.documentElement.scrollTop;
        if (scrolled_e <= 100)
            $('#scroll-nav').removeClass('active');
        else
            $('#scroll-nav').addClass('active');


        var scrolled_350 = window.pageYOffset || document.documentElement.scrollTop;
        if (scrolled_e < 350)
            $('#btn-to-up').removeClass('active');
        else
            $('#btn-to-up').addClass('active');

    };

    //Медленный скролл к якорю
    $(".link-to-article").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });

    $('.burger-btn').on('click', function () {
        $('#mobile-menu').toggleClass('active');
    });

    $('#openCategory').on('click', function () {
        $('#accordionCatalog').toggleClass('active');
    });
    $('.close-mobile-catalog').on('click', function () {
        $('#accordionCatalog').removeClass('active');
    });


    //SLICK
    $('.slick-box').slick({
        infinite: true,
        slidesToShow: 4,
        arrows: true,
        prevArrow: '<button class="slick-prev-b" aria-label="Previous" type="button"><img src="/svg/arrow-left-pink.svg" alt=""></button>',
        nextArrow: '<button class="slick-next-b" aria-label="Next" type="button"><img src="/svg/arrow-right-pink.svg" alt=""></button>',
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1150,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    arrows: false,
                    centerMode: true,
                    centerPadding: '50px'
                }
            },
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    centerMode: true,
                    centerPadding: '50px'
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px'
                }
            }

        ]

    });



    //Анимация для LABEL
    let inputName = document.getElementById('name');
    let phoneName = document.getElementById('phone');
    let countName = document.getElementById('count');
    let commentName = document.getElementById('comment');


    function labelAnimation(className) {
            var name = className.value;
            console.log = ("className = "+ className);

            if (name.length === 0 ) {
                className.innerHTML = '';
                className.classList.remove('full-input');
            } else
                className.classList.add('full-input');

    };

    //
    // inputName.onblur = function() {labelAnimation(inputName)} ;
    // phoneName.onblur = function() {labelAnimation(phoneName)} ;
    // countName.onblur = function() {labelAnimation(countName)} ;
    // commentName.onblur = function() {labelAnimation(commentName)} ;



});
