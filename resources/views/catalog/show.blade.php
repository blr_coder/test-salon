@extends('layouts.layout')

@section('active-class-1', 'active')

@section('background-color', 'background-color')

@section('content')

    @include('partial.nav')


    <section class="pt-40 pb-80">

        <div class="container catalog-page catalog-page__item">

            <div class="breadcrumb-nav">
                <a href="/">Главная</a>
                <a href="/catalog">Каталог</a>
                <a href="/item/{{ $product->slug }}" class="active">{{ $product->title }}</a>
            </div>


            <div class="row pb-80 justify-content-center">

                <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 mb-4 mb-sm-5 mb-xl-0">
                    <div id="carouselCatalogItem" class="carousel slide carousel-fade" data-ride="carousel">
                        <div class="carousel-inner zoom-gallery">
                            <div class="carousel-item active">
                                <a href="/storage/uploaded_images/products/{{ $product->image_1 }}" data-source="/storage/uploaded_images/products/{{ $product->image_1 }}">
                                    <img src="/storage/uploaded_images/products/{{ $product->image_1 }}" class="w-100" alt="{{ $product->title }}">
                                </a>
                            </div>
                            <div class="carousel-item">
                                <a href="/storage/uploaded_images/products/{{ $product->image_2 }}" data-source="/storage/uploaded_images/products/{{ $product->image_2 }}">
                                    <img src="/storage/uploaded_images/products/{{ $product->image_2 }}" class="w-100" alt="{{ $product->title }}">
                                </a>
                            </div>
                            <div class="carousel-item">
                                <a href="/storage/uploaded_images/products/{{ $product->image_3 }}" data-source="/storage/uploaded_images/products/{{ $product->image_3 }}">
                                    <img src="/storage/uploaded_images/products/{{ $product->image_3 }}" class="w-100" alt="{{ $product->title }}">
                                </a>
                            </div>
                        </div>

                        <div class="carousel-indicators">
                            <div data-target="#carouselCatalogItem" data-slide-to="0" class="active indicator">
                                <img src="/storage/uploaded_images/products/{{ $product->image_1 }}" alt="{{ $product->title }}" class="w-100">
                            </div>
                            <div data-target="#carouselCatalogItem" data-slide-to="1" class="indicator">
                                <img src="/storage/uploaded_images/products/{{ $product->image_2 }}" alt="{{ $product->title }}" class="w-100">
                            </div>

                            @if(!empty($product->video))
                                <a href="https://www.youtube.com/watch?v={{ Str::after($product->video, 'youtu.be/') }}=0s" target="_blank" class="indicator indicator-video">
                                    <img src="/svg/play.svg" class="img-fluid img-play" alt="">
                                    <img src="/storage/uploaded_images/products/{{ $product->image_3 }}" class="img-fluid" alt="">

                                </a>
                            @else
                                <div data-target="#carouselCatalogItem" data-slide-to="2" class="indicator">
                                    <img src="/storage/uploaded_images/products/{{ $product->image_3 }}" alt="{{ $product->title }}" class="w-100">
                                </div>
                            @endif

                        </div>

                    </div>
                </div>

                <div class="col-lg-6 col-xl-7">

                    <div class="description-container">

                        <div>
                            <h1>
                                {{ $product->title }}
                            </h1>
                            <div class="status color-second">
                                @switch ( $product->status )
                                    @case(1)
                                    В наличии
                                    @break
                                    @case(2)
                                    Скоро в наличии
                                    @break
                                    @case(3)
                                    Под заказ
                                    @break
                                @endswitch
                            </div>
                        </div>


                        <div class="model-box">
                            <div>
                                <span class="d-inline-block mb-2">Модель:</span>
                                <span class="color-second">{{ $product->model }}</span>
                            </div>
                            <div >
                                Дизайнер:
                                <span class="color-second">{{ $product->designer->title }}</span>
                            </div>
                        </div>

                        <!--Price Box IF ITEM HAS DISCOUNT SHOW-->
                        <div class="with-discount d-flex align-items-center pl-0">
                            <span class="mr-3">Цена:</span>

                            @if(!empty($product->discount_price))
                                <div class="mb-0 old-price mr-2">
                                    {{ $product->price }}
                                </div>
                                <h6 class="main-color mb-0">{{ $product->discount_price }} BYN</h6>

                            @else
                                <h6 class="main-color mb-0">{{ $product->price }} BYN</h6>
                            @endif
                        </div>


                        <div class="mb-4 mt-4">

                                @if(!empty($product->sizes))

                                    <p class="mb-2">Размеры в наличии:</p>
                                    <div class="d-flex sizes-box mb-3">

                                        @foreach($product->sizes as $size)
                                            <div class="active">{{ $size }}</div>
                                        @endforeach

                                    </div>

                                @endif

                            <p class="mb-2">Цвета в наличии:</p>
                            <div class="status color-second">
                                    {{ $product->colors }}
                            </div>
                        </div>



                        <div>
                            <p class="mb-2">Краткое описание:</p>
                            <div class="mb-0 short-description">
                                {!! $product->description !!}
                            </div>
                        </div>

                        <div class="info-text mb-3 mt-3">
                            <p class="mb-0">
                                Мы гарантируем подлинность и качество продукции, представленной в нашем салоне. На все модели платьев имеются документы официального производителя и сертификаты качества в соответствии
                                с требованиями ТРСТ. Копии сертификатов находятся в магазине и доступны к просмотру!
                            </p>
                        </div>

                        @if(!empty($product->show_recommendations))
                            <div class="d-flex justify-content-between">
                                <button type="button" class="main-btn" data-toggle="modal" data-target="#catalogModal1">Хранение</button>
                                <button type="button" class="main-btn" data-toggle="modal" data-target="#catalogModal2">Химчистка</button>
                                <button type="button" class="main-btn" data-toggle="modal" data-target="#catalogModal3">Реставрация</button>
                            </div>
                        @endif

                    </div>



                </div>

            </div>


        </div>

    </section>




    <div class="container pb-80">
        <div class="w-60 text-center mb-100">
            <h3>
                    <span class="title-line">
                    рекомендуем для вас
                    </span>
            </h3>

        </div>

        <div class="slick-box__container">
            <div class="slick-box">

                @if( count($product->category->products) > 4 )

                    @foreach($product->category->products->except([$product->id])->random(4) as $otherProduct)
                        <div class="slick-box__card">
                            <div class="item--card">
                                <a href="{{ action('ItemController@show', [$otherProduct->slug]) }}">
                                    <div class="top-container">
                                        <img src="/storage/uploaded_images/products/{{ $otherProduct->image_1 }}" alt="{{ $otherProduct->title }}" class="w-100">
                                    </div> <div class="bottom-container">
                                        <div class="d-sm-flex justify-content-between">
                                            <div class="mb-2 mb-sm-0">
                                                <h4>{{ $otherProduct->title }}</h4>
                                                <div class="in-stock-box">
                                                    @switch ( $otherProduct->status )
                                                        @case(1)
                                                        В наличии
                                                        @break
                                                        @case(2)
                                                        Скоро в наличии
                                                        @break
                                                        @case(3)
                                                        Под заказ
                                                        @break
                                                    @endswitch
                                                </div>
                                            </div>
                                            <div class="with-discount text-sm-right">

                                                @if(!empty($otherProduct->discount_price))
                                                    <h6 class="new-price">{{ $otherProduct->discount_price }} BYN</h6>
                                                    <div class="old-price">
                                                        {{ $otherProduct->price }} BYN
                                                    </div>
                                                @else
                                                    <h6 class="new-price">{{ $otherProduct->price }} BYN</h6>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-btn w-100">Подробнее</div>
                                </a>
                            </div>
                        </div>
                    @endforeach

                @else

                    @foreach(\App\Models\Product::all()->except([$product->id])->random(4) as $otherProduct)
                        <div class="slick-box__card">
                            <div class="item--card">
                                <a href="{{ action('ItemController@show', [$otherProduct->slug]) }}">
                                    <div class="top-container">
                                        <img src="/storage/uploaded_images/products/{{ $otherProduct->image_1 }}" alt="{{ $otherProduct->title }}" class="w-100">
                                    </div> <div class="bottom-container">
                                        <div class="d-sm-flex justify-content-between">
                                            <div class="mb-2 mb-sm-0">
                                                <h4>{{ $otherProduct->title }}</h4>
                                                <div class="in-stock-box">
                                                    @switch ( $otherProduct->status )
                                                        @case(1)
                                                        В наличии
                                                        @break
                                                        @case(2)
                                                        Скоро в наличии
                                                        @break
                                                        @case(3)
                                                        Под заказ
                                                        @break
                                                    @endswitch
                                                </div>
                                            </div>
                                            <div class="with-discount text-sm-right">

                                                @if(!empty($otherProduct->discount_price))
                                                    <h6 class="new-price">{{ $otherProduct->discount_price }} BYN</h6>
                                                    <div class="old-price">
                                                        {{ $otherProduct->price }} BYN
                                                    </div>
                                                @else
                                                    <h6 class="new-price">{{ $otherProduct->price }} BYN</h6>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="main-btn w-100">Подробнее</div>
                                </a>
                            </div>
                        </div>
                    @endforeach

                @endif

            </div>
        </div>


    </div>



    @include('modals.catalogItem')
    @include('partial.footer')

@endsection
