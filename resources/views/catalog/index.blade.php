@extends('layouts.layout')

@section('active-class-1', 'active')

@section('background-color', 'background-color')

@section('content')

    @include('partial.nav')

{{--    <div>КАТЕГОРИЯ  = @if(!empty($categoryId)) {{ $categoryId }} @endif, ПОДКАТЕГОРИЯ = @if(!empty($subCategoryId)) {{ $subCategoryId }} @endif</div>--}}

    <items-catalog
        @if(!empty(request('search-tag'))) search="{{ request('search-tag') }}" @endif
        @if(!empty($categoryId)) selected_category_id="{{ $categoryId }}" @endif
        @if(!empty($subCategoryId)) selected_sub_category_id="{{ $subCategoryId }}" @endif
        @if(!empty($staticLink)) selected_static_link="{{ $staticLink }}" @endif

    >

    </items-catalog>



    @include('partial.footer')

@endsection
