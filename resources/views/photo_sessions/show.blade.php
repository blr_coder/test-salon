@extends('layouts.layout')

@section('active-class-3', 'active')

@section('content')

    @include('partial.nav')

    <section class="article-page photo-page_show">
        <div id="main-banner">

            <img src="/storage/uploaded_images/albums/{{ $album->image_for_desktop }}" alt="{{ $album->title }}" class="w-100 ">

            <div class="info-box ">
                <h3>{{ $album->photographer }}</h3>
                <p>{{ $album->location }}</p>

            </div>

        </div>
    </section>

    <section class="photo-page">
        <div class="container">
            <div class="breadcrumb-nav">
                <a href="/">Главная</a>
                <a href="/photo_sessions">Фотосессии</a>
                <a href="/photo_sessions/{{ $album->slug }}" class="active">{{ $album->title }}</a>
            </div>

            <div class="w-60">
                <h1 class="text-center mb-50">
                    <span class="title-line">
                        {{ $album->title }}
                    </span>

                </h1>
                <p class="font-light text-center mb-30">
                    Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами.
                </p>
            </div>

            <div class="row mb-50">

                @foreach($album->photos as $photo)
                    <div class="col-4 col-md-3 mb-4">
                        <div class="zoom-gallery">

                            <a href="/storage/uploaded_images/photos/{{ $photo->image }}" data-source="/img/del/banner.png">
                                <img src="/storage/uploaded_images/photos/small/{{ $photo->image_small }}" class="w-100" alt="{{ $album->title }}">
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

    @include('partial.footer')

@endsection
