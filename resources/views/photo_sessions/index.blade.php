@extends('layouts.layout')

@section('active-class-3', 'active')

@section('content')

    @include('partial.nav')

    @if(!empty($albums[0]))
        <section class="article-page">
            <div id="main-banner">

                <img src="/storage/uploaded_images/albums/{{ $albums[0]->image_for_desktop }}" alt="{{ $albums[0]->title }}" class="w-100 ">

                <div class="info-box text-center w-60">
                    <h1 class="mb-30">{{ $albums[0]->title }}</h1>

                    <a href="/photo_sessions/{{ $albums[0]->slug }}">
                        <div class="btn-second">
                            Перейти в альбом
                        </div>
                    </a>

                </div>

            </div>
        </section>
    @endif

    <section class="photo-page">
        <div class="container">
            <div class="breadcrumb-nav">
                <a href="/">Главная</a>
                <a href="/photo_sessions" class="active">Фотосессии</a>
            </div>

            <div class="w-60">
                <h3 class="text-center mb-50">
                    <span class="title-line">
                        фотосессии
                    </span>

                </h3>
                <p class="preview-text font-light text-center mb-30">
                    «Все изображения защищены авторским правом. Любое использование либо копирование материалов или подборки материалов сайта, элементов дизайна и оформления допускается лишь с разрешения правообладателя и только со ссылкой на источник: www.salon-sensation.by»
                </p>
            </div>

            <div class="row mb-50">

                @foreach($albums as $album)
                    <div class="col-6 col-md-4 col-lg-3 mb-4">
                        <a href="/photo_sessions/{{ $album->slug }}">
                            <div class="photo-card">
                                <img src="/storage/uploaded_images/albums/small/{{ $album->image_for_desktop_small }}" alt="{{ $album->title }}" class="w-100">
                                <h5 class="title-box">
                                    {{ $album->title }}
                                </h5>
                                <div class="hidden-box">
                                    Посмотреть все фотографии
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>

    </section>

    @include('partial.footer')

@endsection
