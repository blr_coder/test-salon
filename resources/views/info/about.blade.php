@extends('layouts.layout')

@section('active-class-2', 'active')

@section('content')

    @include('partial.nav')

    <section class="article-page about-page">
        <div id="main-banner">

{{--            <img src="/img/info/about.jpg" alt="" class="w-100">--}}

            <div class="info-box text-center w-60">
                <h2 class="mb-4 banner">sensation</h2>

                <p class="text-center">Мультибрендовый бутик эксклюзивных платьев от дизайнеров с мировым именем!</p>
            </div>

        </div>
    </section>

    <section class="about-page pb-80">
        <div class="container">
            <div class="breadcrumb-nav">
                <a href="/">Главная</a>
                <a href="/about" class="active">О салоне</a>
            </div>

            <div class="w-60">
                <h1 class="text-center mb-50">
                    <span class="title-line">
                        о салоне
                    </span>

                </h1>

                <h4 class="text-center mb-4 font-weight-normal">
                    В магазине представлены коллекции вечерних и коктейльных платьев брендов Sherri Hill, Tarik Ediz, Terani Сouture.
                </h4>



                <p class="font-light text-center mb-50">
                    Мы - магазин-мультибренд, в то время как по миру эти дизайнеры чаще всего представлены в конкретных разных бутиках. Объединяя под одной крышей несколько брендов, мы действуем в интересах покупателя, давая ему возможность выбирать из лучшего, экономя время и силы.
                </p>
            </div>

            <div class="row justify-content-center position-relative">

                <div class="wow overlay" data-wow-duration="2s" data-wow-delay="15ms" data-wow-offset="10"></div>

                <div class="col-sm-10 col-lg-7 col-xl-4 mb-4 mb-xl-0">

                    <div class="about__card">
                        <h5 class="text-center">SHERRI HILL</h5>

                        <p>
                            Платья Sherri Hill – <span class="font-light">для смелых, уверенных в себе девушек, любящих дорогой блеск и качественные ткани.
                            Найти аналоги бренда невозможно, поэтому сегодня он очень высоко ценится.
                            Sensation – является единственным официальным представителем дизайнера в стране.</span>
                        </p>
                        <p class="font-light">
                            В настоящее время семейный бизнес Sherri Hill насчитывает более 1000 магазинов, продающих бренд в более чем 52 странах.
                        </p>

                        <p class="mb-0">
                            <span class="font-light">Sherri Hill отпраздновал своё 11-летие! Из них вот уже 6 лет наш салон единственный официально представляет этот бренд на территории Беларуси!</span>
                            <button type="button" data-toggle="modal" data-target="#aboutModal1"><span class="main-color btn-next">Подробнее</span></button>

                        </p>
                    </div>

                </div>


                <div class="col-sm-10 col-lg-7 col-xl-4 mb-4 mb-xl-0">

                        <div class="about__card">
                            <h5 class="text-center">TARIK EDIZ</h5>

                            <p>
                                Tarik Ediz <span class="font-light">– дизайнер, завоевавший доверие клиентов на 5 континентах и в 95 странах мира. Особенную популярность турецкий бренд приобрел в Италии благодаря высококачественным тканям и фурнитуре, а также соответствию европейским стандартам пошива платьев. </span>
                            </p>
                            <p class="mb-2 main-color">
                                100% ручная работа
                            </p>
                            <p class="mb-2 main-color">
                                Собственная фабрика в Стамбуле
                            </p>
                            <p class="main-color">
                                Возможность отшить платье под заказ в любом размере
                            </p>

                            <p class="mb-0">
                                <span class="font-light">Tarik Ediz стал столь близок европейцам за счет сдержанных коллекций платьев, отвечающих последним тенденциям вечерней моды. </span>
                                <button type="button" data-toggle="modal" data-target="#aboutModal2"><span class="main-color btn-next">Подробнее</span></button>
                            </p>
                        </div>

                </div>


                <div class="col-sm-10 col-lg-7 col-xl-4 mb-4 mb-xl-0">

                        <div class="about__card">
                            <h5 class="text-center">TERANI COUTURE</h5>

                            <p class="font-light">
                                Платья Terani Сouture носят женщины по всему миру. Независимо от стиля, формы тела, размера женщины, в салоне можно найти красивейшие коллекции платьев из специальных линеек «мама невесты и жениха».
                            </p>


                            <p class="mb-0">
                                <span>Terani Сouture</span>
                                <span class="font-light">начала свою деятельность как компания по производству женских костюмов в швейном районе Нью-Йорка в 1990 году, а затем превратилась в бренд для особых случаев. И представила коллекции, которые в настоящее время варьируются от выпускных платьев, коктейльных, платьев подружки невесты до платьев мамы невесты.</span>
                                <button type="button" data-toggle="modal" data-target="#aboutModal3"><span class="main-color btn-next">Подробнее</span></button>
                            </p>
                        </div>

                </div>

            </div>
        </div>
    </section>

    @include('modals.aboutModals')
    @include('partial.footer')

@endsection
