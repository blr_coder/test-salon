@extends('layouts.admin')

@section('title')
    <title>Admin panel - Категории</title>
@endsection

@section('content')


    <section id="admin" class="admin-articles">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>

                            <li class="breadcrumb-item active" aria-current="page">Категории</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-sm-4 text-right mt-3">
                    <a href="{{ action('Admin\CategoryController@create') }}" class="btn-primary">Добавить категорию</a>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Название категории</th>
                            <th>Slug</th>
                            <th>Порядок отображения</th>
                            <th>Товары</th>
                            <th class="text-center">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mainCategories as $mainCategory)
                            <tr class="bg-light border">
                                <td style="width: 30%">{{ mb_strtoupper($mainCategory->title) }}</td>
                                <td style="width: 25%">{{ $mainCategory->slug }}</td>
                                <td style="width: 15%">{{ $mainCategory->number }}</td>
                                <td style="width: 10%">
                                    @foreach($mainCategory->products as $product)
                                        <a href="{{ action('ItemController@show', [$product->slug]) }}">{{ $product->title }}</a>
                                    @endforeach
                                </td>
                                <td style="width: 20%">
                                    <div class="row">

                                        <div class="col lg-6 sm-12">
{{--                                            <a href="{{ action('Admin\CategoryController@edit', [$mainCategory]) }}" class="btn btn-sm admin_edit btn-block">Редактировать</a>--}}
                                            <a href="{{ action('Admin\CategoryController@edit', [$mainCategory]) }}">
                                                <button class="btn-edit">Редактировать</button>
                                            </a>
                                        </div>

                                        <div class="col lg-6 sm-12">
                                            <form method="post" action="{{ action('Admin\CategoryController@delete', [$mainCategory]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <div>
                                                    <button type="submit" onclick="return confirm('Вы уверены, что хотите удалить категорию, со всеми вложенными подкатегориями?')" class="btn-remove">Удалить</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                            @foreach($mainCategory->subcategories as $subcategory)
                                <tr class="bg-white">
                                    <td class="pl-4 text-center" style="width: 30%"> ● {{ $subcategory->title }}</td>
                                    <td style="width: 25%">{{ $subcategory->slug }}</td>
                                    <td style="width: 15%">{{ $subcategory->number }}</td>
                                    <td style="width: 10%">
                                        @foreach($subcategory->products as $product)
                                            <a href="{{ action('ItemController@show', [$product->slug]) }}">{{ $product->title }}</a>
                                        @endforeach
                                    </td>
                                    <td style="width: 30%">
                                        <div class="row">

                                            <div class="col lg-6 sm-12">
{{--                                                <a href="{{ action('Admin\CategoryController@edit', [$subcategory]) }}" class="btn btn-sm btn-block">Редактировать</a>--}}
                                                <a href="{{ action('Admin\CategoryController@edit', [$subcategory]) }}">
                                                    <button class="btn-edit">Редактировать</button>
                                                </a>
                                            </div>

                                            <div class="col lg-6 sm-12">
                                                <form method="post" action="{{ action('Admin\CategoryController@delete', [$subcategory]) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div>
                                                        <button type="submit" onclick="return confirm('Вы уверены, что хотите удалить категорию?')" class="btn-remove">Удалить</button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>

    </section>
@endsection
