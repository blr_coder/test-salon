@extends('layouts.admin')

@section('content')

    <section id="admin" class="admin-articles">
        <div class="container">

            <div class="row pt-5">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>
                            <li class="breadcrumb-item"><a href="{{ action('Admin\CategoryController@index') }}">Категории</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Добавление категории</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-sm-12 text-center">
                    <h5>Добавление категории на ресурсе</h5>
                </div>
            </div>

            <form action="{{ action('Admin\CategoryController@store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="form-group col-lg-12 col-12">
                        <label for="title">Название категории</label>
                        <input type="text" class="form-control" id="title" name="title" maxlength="190" required>
                    </div>

                    <div class="form-group col-lg-12 col-12">
                        <label for="parent_id">Вложенность</label>
                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="0">Сделать основной категорией</option>

                            @foreach($mainCategories as $mainCategory)
                                <option value="{{ $mainCategory->id }}">{{ $mainCategory->title }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <hr>

                <div class="row pb-5">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn-create">Создать</button>
                    </div>
                </div>

            </form>

        </div>
    </section>

@stop

