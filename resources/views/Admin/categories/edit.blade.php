@extends('layouts.admin')

@section('content')

    <section id="admin" class="admin-articles">
        <div class="container">

            <div class="row pt-5">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>
                            <li class="breadcrumb-item"><a href="{{ action('Admin\CategoryController@index') }}">Категории</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Редактирование категории</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-sm-12 text-center">
                    <h5>Редактирование категории {{ $category->title }}</h5>
                </div>
            </div>

            <form action="{{ action('Admin\CategoryController@update', [$category]) }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="form-group col-lg-12 col-12">
                        <label for="title">Название категории</label>
                        <input type="text" class="form-control" id="title" name="title" maxlength="190" required value="{{ $category->title }}">
                    </div>

                    <div class="form-group col-lg-12 col-12">
                        <label for="slug">SLUG</label>
                        <input type="text" class="form-control" id="slug" name="slug" maxlength="190" required value="{{ $category->slug }}">
                    </div>

                    @if(!$category->isMain())
                        <div class="form-group col-lg-12 col-12">
                            <label for="parent_id">Вложенность</label>
                            <select name="parent_id" id="parent_id" class="form-control">
                                <option value="0">Сделать основной категорией</option>

                                @foreach($mainCategories as $mainCategory)
                                    <option value="{{ $mainCategory->id }}" @if($category->parent->id == $mainCategory->id) selected @endif >{{ $mainCategory->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    @elseif(count($category->subcategories) == 0)
                        <div class="form-group col-lg-12 col-12">
                            <label for="parent_id">Вложенность</label>
                            <select name="parent_id" id="parent_id" class="form-control">
                                @foreach($mainCategories as $mainCategory)
                                    <option value="{{ $mainCategory->id }}" >{{ $mainCategory->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                </div>
                <hr>

                <div class="row pb-5">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn-save">Сохранить</button>
                    </div>
                </div>

            </form>

        </div>
    </section>

@stop

