@extends('layouts.admin')

@section('content')

    <section id="admin" class="admin-articles">
        <div class="container">

            <div class="row pt-5">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>
                            <li class="breadcrumb-item"><a href="{{ action('Admin\ArticleController@index') }}">Новости</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Редактирование новости</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 text-center">
                    <h5>Редактирование новости "{{ $article->title }}" на ресурсе</h5>
                </div>
            </div>

            <form action="{{ action('Admin\ArticleController@update', [$article]) }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="form-group col-lg-5 col-12">
                        <label for="title">Наименование</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" maxlength="190" required value="{{ $article->title }}">
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-5 col-12">
                        <label for="slug">SLUG</label>
                        <input type="text" class="form-control @error('slug') is-invalid @enderror" id="slug" name="slug" maxlength="190" required value="{{ $article->slug }}">
                        @error('slug')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('slug') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-2 col-12">
                        <label for="date">Дата (может быть пустой)</label>
                        <input type="date" class="form-control @error('date') is-invalid @enderror" id="date" name="date" value="{{ $article->date }}">
                        @error('date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('date') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-12 col-12">
                        <label for="preview1">Превью</label>
                        <textarea name="preview" id="preview1" maxlength="500" class="form-control @error('preview') is-invalid @enderror" cols="10" required>
                            {{ $article->preview }}
                        </textarea>
                        @error('preview')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('preview') }}</strong>
                            </span>
                        @enderror
                    </div>




                    <div class="form-group col-lg-6 col-12">
                        <label for="old_image_for_desktop">Изображение для больших экранов сейчас</label>
                        <img src="/storage/uploaded_images/articles/{{ $article->image_for_desktop }}" alt="img" class="img-fluid" id="old_image_for_desktop" style="width: 25%;">
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="desktop_image_file">Для замены изображения для больших экранов</label>
                        <input type="file" class="form-control @error('desktop_image_file') is-invalid @enderror" id="desktop_image_file" name="desktop_image_file">
                        @error('desktop_image_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('desktop_image_file') }}</strong>
                            </span>
                        @enderror
                        <p class="help-block my-4">Файл будет приведен к размерам 1920px * 830px</p>
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="old_image_for_mobile">Изображение для мобильных устройств сейчас</label>
                        <img src="/storage/uploaded_images/articles/mobile/{{ $article->image_for_mobile }}" alt="img" class="img-fluid" id="old_image_for_mobile" style="width: 25%;">
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="mobile_image_file">Для замены изображения для мобильных устройств</label>
                        <input type="file" class="form-control @error('mobile_image_file') is-invalid @enderror" id="mobile_image_file" name="mobile_image_file">
                        @error('desktop_image_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('desktop_image_file') }}</strong>
                            </span>
                        @enderror
                        <p class="help-block my-4">Файл будет приведен к размерам 506px</p>
                    </div>




                    <div class="form-group col-sm-12">
                        <label for="description">Описание</label>
                        <textarea name="description" id="description" maxlength="500" class="form-control tinymce @error('description') is-invalid @enderror" cols="10" style="min-height: 160px">
                            {{ $article->description }}
                        </textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @enderror
                        <p class="text-center">Для адаптации картинки по ширине экрана - 100% - auto</p>
                    </div>

                </div>
                <hr>

                <div class="row pb-5">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn-save">Сохранить</button>
                    </div>
                </div>

            </form>

        </div>
    </section>

@stop

