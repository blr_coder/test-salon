@extends('layouts.admin')

@section('title')
    <title>Admin panel - Новости</title>
@endsection

@section('content')


    <section id="admin" class="admin-articles">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>

                            <li class="breadcrumb-item active" aria-current="page">Новости</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-sm-4 text-right mt-2">
                    <a href="{{ action('Admin\ArticleController@create') }}" class="btn-primary">Добавить новость</a>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row pt-2">

                <div class="col-sm-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 150px">Изображение для превью</th>
                            <th style="width: 150px">Изображение для мобильных устройств</th>
                            <th>Название</th>
                            <th>Превью</th>
                            <th>Описание</th>
                            <th>Дата</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td style="width: 19%"><img src="/storage/uploaded_images/articles/small/{{ $article->image_for_desktop_small }}" class="img-fluid" alt="{{ $article->title }}"></td>
                                <td style="width: 11%"><img src="/storage/uploaded_images/articles/mobile/{{ $article->image_for_mobile }}" class="img-fluid" alt="{{ $article->title }}"></td>
                                <td style="width: 10%">{{ mb_strtoupper($article->title) }}</td>
                                <td style="width: 20%">{!! Illuminate\Support\Str::limit($article->preview, $limit = 200, $end = '...') !!}</td>
                                <td style="width: 35%">{!! Illuminate\Support\Str::limit($article->description, $limit = 350, $end = '...') !!}</td>
                                <td style="width: 10%">{{ date('d.m.Y', strtotime($article->date)) }}</td>
                                <td style="width: 10%">
                                    <div class="row">

                                        <div class="col-12">
                                            <a href="{{ action('Admin\ArticleController@edit', [$article]) }}">
                                                <button class="btn-edit">Редактировать</button>
                                            </a>
                                        </div>

                                        <div class="col-12">
                                            <form method="post" action="{{ action('Admin\ArticleController@delete', [$article]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <div>
                                                    <button type="submit" onclick="return confirm('Вы уверены, что хотите удалить статью?')" class="btn-remove w-100">Удалить</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </section>
@endsection
