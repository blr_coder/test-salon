@extends('layouts.admin')

@section('content')

    <section id="admin" class="admin-articles">
        <div class="container">

            <div class="row pt-5">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>
                            <li class="breadcrumb-item"><a href="{{ action('Admin\ProductController@index') }}">Продукты</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Добавление продукта</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-sm-12 text-center">
                    <h5>Создание нового продукта</h5>
                </div>
            </div>

            <form action="{{ action('Admin\ProductController@store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="form-group col-lg-6 col-12">
                        <label for="title">Наименование</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" maxlength="190" required>
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="model">Модель</label>
                        <input type="text" class="form-control @error('model') is-invalid @enderror" id="model" name="model" maxlength="190" required>
                        @error('model')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('model') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="price">Цена</label>
                        <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" maxlength="190" required>
                        @error('price')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('price') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="discount_price">Цена со скидкой (может быть пустой)</label>
                        <input type="text" class="form-control @error('discount_price') is-invalid @enderror" id="discount_price" name="discount_price" maxlength="190">
                        @error('discount_price')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('discount_price') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="text-center col-sm-12 my-4">Выберите изображения для товара (Файлы будут приведены к размерам 400px * 615px)</div>

                    <div class="form-group col-lg-4 col-12">
                        <label for="image_1_file">Изображение №1</label>
                        <input type="file" class="form-control @error('image_1_file') is-invalid @enderror" id="image_1_file" name="image_1_file" required>
                        @error('image_1_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image_1_file') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-4 col-12">
                        <label for="image_2_file">Изображение №2</label>
                        <input type="file" class="form-control @error('image_2_file') is-invalid @enderror" id="image_2_file" name="image_2_file" required>
                        @error('image_2_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image_2_file') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-4 col-12">
                        <label for="image_3_file">Изображение №3</label>
                        <input type="file" class="form-control @error('image_3_file') is-invalid @enderror" id="image_3_file" name="image_3_file" required>
                        @error('image_3_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image_3_file') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-12 col-12">
                        <label for="video">Короткая ссылка на видео (может отсутствовать)</label>
                        <input type="text" class="form-control @error('video') is-invalid @enderror" name="video" id="video">
                        @error('video')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('video') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-4 col-12">
                        <label for="status">Статус</label>

                        <select name="status" id="status" class="form-control @error('status') is-invalid @enderror" required>
                            <option value="1">В наличии</option>
                            <option value="2">Скоро в наличии</option>
                            <option value="3">Под заказ</option>
                        </select>
                        @error('status')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-4 col-1">
                        <div class="mb-3">
                            <input type="radio" id="new" name="new_or_special_price" value="1" class="d-inline-block w-auto">
                            <label for="new" class="mb-0 d-inline-block ml-2">Новый</label>
                        </div>

                        <div>
                            <input type="radio" id="special_price" name="new_or_special_price" value="2" class="d-inline-block w-auto">
                            <label for="special_price" class="d-inline-block ml-2">Спец цена</label>
                        </div>

                    </div>




                    <div class="form-group col-12">
                        <p>Размеры</p>
                        <div class="d-flex checkbox mb-3">
                            <input type="checkbox" name="sizes[{{ 0 }}]" id="size-0" value="00">
                            <label for="size-0" class="mr-4">00</label>

                            <input type="checkbox" name="sizes[{{ 1 }}]" id="size-1" value="0">
                            <label for="size-1" class="mr-4">0</label>

                            <input type="checkbox" name="sizes[{{ 2 }}]" id="size-2" value="2">
                            <label for="size-2" class="mr-4">2</label>

                            <input type="checkbox" name="sizes[{{ 3 }}]" id="size-3" value="4">
                            <label for="size-3" class="mr-4">4</label>

                            <input type="checkbox" name="sizes[{{ 4 }}]" id="size-4" value="6">
                            <label for="size-4" class="mr-4">6</label>

                            <input type="checkbox" name="sizes[{{ 5 }}]" id="size-5" value="8">
                            <label for="size-5" class="mr-4">8</label>

                            <input type="checkbox" name="sizes[{{ 6 }}]" id="size-6" value="10">
                            <label for="size-6" class="mr-4">10</label>

                            <input type="checkbox" name="sizes[{{ 7 }}]" id="size-7" value="12">
                            <label for="size-7" class="mr-4">12</label>

                            <input type="checkbox" name="sizes[{{ 8 }}]" id="size-8" value="14">
                            <label for="size-8" class="mr-4">14</label>

                            <input type="checkbox" name="sizes[{{ 9 }}]" id="size-9" value="16">
                            <label for="size-9" class="mr-4">16</label>

                            <input type="checkbox" name="sizes[{{ 10 }}]" id="size-10" value="18">
                            <label for="size-10" class="mr-4">18</label>

                            <input type="checkbox" name="sizes[{{ 11 }}]" id="size-11" value="20">
                            <label for="size-11" class="mr-4">20</label>
                        </div>

                        <div class="d-flex checkbox mb-3">
                            <input type="checkbox" name="sizes[{{ 12 }}]" id="size-12" value="XXS">
                            <label for="size-12" class="mr-4">XXS</label>

                            <input type="checkbox" name="sizes[{{ 13 }}]" id="size-13" value="XS">
                            <label for="size-13" class="mr-4">XS</label>

                            <input type="checkbox" name="sizes[{{ 14 }}]" id="size-14" value="S">
                            <label for="size-14" class="mr-4">S</label>

                            <input type="checkbox" name="sizes[{{ 15 }}]" id="size-15" value="M">
                            <label for="size-15" class="mr-4">M</label>

                            <input type="checkbox" name="sizes[{{ 16 }}]" id="size-16" value="L">
                            <label for="size-16" class="mr-4">L</label>

                            <input type="checkbox" name="sizes[{{ 17 }}]" id="size-17" value="XL">
                            <label for="size-17" class="mr-4">XL</label>

                            <input type="checkbox" name="sizes[{{ 18 }}]" id="size-18" value="XXL">
                            <label for="size-18" class="mr-4">XXL</label>

                            <input type="checkbox" name="sizes[{{ 19 }}]" id="size-19" value="XXXL">
                            <label for="size-19" class="mr-4">XXXL</label>
                        </div>

                        <div class="d-flex checkbox mb-3">
                            <input type="checkbox" name="sizes[{{ 20 }}]" id="size-20" value="32">
                            <label for="size-20" class="mr-4">32</label>

                            <input type="checkbox" name="sizes[{{ 21 }}]" id="size-21" value="34">
                            <label for="size-21" class="mr-4">34</label>

                            <input type="checkbox" name="sizes[{{ 22 }}]" id="size-22" value="36">
                            <label for="size-22" class="mr-4">36</label>

                            <input type="checkbox" name="sizes[{{ 23 }}]" id="size-23" value="38">
                            <label for="size-23" class="mr-4">38</label>

                            <input type="checkbox" name="sizes[{{ 24 }}]" id="size-24" value="40">
                            <label for="size-24" class="mr-4">40</label>

                            <input type="checkbox" name="sizes[{{ 25 }}]" id="size-25" value="42">
                            <label for="size-25" class="mr-4">42</label>

                            <input type="checkbox" name="sizes[{{ 26 }}]" id="size-26" value="44">
                            <label for="size-26" class="mr-4">44</label>

                            <input type="checkbox" name="sizes[{{ 27 }}]" id="size-27" value="46">
                            <label for="size-27" class="mr-4">46</label>

                            <input type="checkbox" name="sizes[{{ 28 }}]" id="size-28" value="48">
                            <label for="size-28" class="mr-4">48</label>

                            <input type="checkbox" name="sizes[{{ 29 }}]" id="size-29" value="50">
                            <label for="size-29" class="mr-4">50</label>

                        </div>

                    </div>


                    <div class="form-group col-lg-6 col-12">
                        <label for="designer_id">Выберите дизайнера</label>
                        <select name="designer_id" id="designer_id" class="form-control">
                            @foreach($designers as $designer)
                                <option value="{{ $designer->id }}">{{ $designer->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="category_id">Выберите категорию</label>
                        <select name="category_id" id="category_id" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @if(!empty($category->subcategories))
                                    @foreach($category->subcategories as $subcategory)
                                        <option value="{{ $subcategory->id }}"> ● {{ $subcategory->title }}</option>
                                    @endforeach
                                @endif
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group col-lg-8 col-12">
                        <label for="colors">Цвета</label>
                        <input type="text" class="form-control @error('colors') is-invalid @enderror" name="colors" id="colors">
                        @error('colors')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('colors') }}</strong>
                            </span>
                        @enderror
                    </div>




                    <div class="form-group col-lg-4 col-12 checkbox">
                        <input type="checkbox" class="form-check-input" id="show_recommendations" name="show_recommendations" value="1" checked>
                        <label for="show_recommendations">ХРАНЕНИЕ/ХИМЧИСТКА/РЕСТАВРАЦИЯ</label>
                    </div>






                    <div class="form-group col-sm-12">
                        <label for="description">Описание</label>
                        <textarea name="description" id="description" maxlength="500" class="form-control tinymce @error('description') is-invalid @enderror" cols="10" style="min-height: 160px"></textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @enderror
                        <p class="text-center">Для адаптации картинки по ширине экрана - 100% - auto</p>
                    </div>


                </div>
                <hr>

                <div class="row pb-5">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn-create mb-5">Создать</button>
                    </div>
                </div>

            </form>

        </div>
    </section>

@stop

