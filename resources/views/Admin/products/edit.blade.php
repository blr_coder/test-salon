@extends('layouts.admin')

@section('content')

    <section id="admin" class="admin-articles">
        <div class="container">

            <div class="row pt-5">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>
                            <li class="breadcrumb-item"><a href="{{ action('Admin\ProductController@index') }}">Продукты</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Редактирование продукта</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-sm-12 text-center">
                    <h5>Редактирование продукта "{{ $product->title }}"</h5>
                </div>
            </div>

            <form action="{{ action('Admin\ProductController@update', [$product]) }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="form-group col-lg-6 col-12">
                        <label for="title">Наименование</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" maxlength="190" required value="{{ $product->title }}">
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="model">Модель</label>
                        <input type="text" class="form-control @error('model') is-invalid @enderror" id="model" name="model" maxlength="190" required value="{{ $product->model }}">
                        @error('model')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('model') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="price">Цена</label>
                        <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" maxlength="190" required value="{{ $product->price }}">
                        @error('price')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('price') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="discount_price">Цена со скидкой</label>
                        <input type="text" class="form-control @error('discount_price') is-invalid @enderror" id="discount_price" name="discount_price" maxlength="190" value="{{ $product->discount_price }}">
                        @error('discount_price')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('discount_price') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="text-center col-sm-12 my-4">Изображения для товара (Файлы будут приведены к размерам 400px * 615px)</div>


                    <div class="form-group col-12 row">

                        <div class="col-lg-4 col-12 border text-center">
                                <label for="old_image_1" class="col-12">Изображение №1 сейчас</label>
                                <img src="/storage/uploaded_images/products/{{ $product->image_1 }}" alt="img" class="img-fluid" id="old_image_1" style="width: 50%;">

                                <label for="image_1_file">Для замены изображения №1</label>
                                <input type="file" class="form-control my-1 @error('image_1_file') is-invalid @enderror" id="image_1_file" name="image_1_file">
                                @error('image_1_file')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image_1_file') }}</strong>
                                </span>
                                @enderror
                        </div>

                        <div class="col-lg-4 col-12 border text-center">
                            <label for="old_image_2" class="col-12">Изображение №2 сейчас</label>
                            <img src="/storage/uploaded_images/products/{{ $product->image_2 }}" alt="img" class="img-fluid" id="old_image_2" style="width: 50%;">

                            <label for="image_2_file">Для замены изображения №2</label>
                            <input type="file" class="form-control my-1 @error('image_2_file') is-invalid @enderror" id="image_2_file" name="image_2_file">
                            @error('image_2_file')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image_2_file') }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-lg-4 col-12 border text-center">
                            <label for="old_image_3" class="col-12">Изображение №3 сейчас</label>
                            <img src="/storage/uploaded_images/products/{{ $product->image_3 }}" alt="img" class="img-fluid" id="old_image_3" style="width: 50%;">

                            <label for="image_3_file">Для замены изображения №2</label>
                            <input type="file" class="form-control my-1 @error('image_3_file') is-invalid @enderror" id="image_3_file" name="image_3_file">
                            @error('image_3_file')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('image_3_file') }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>




                    <div class="form-group col-lg-8 col-12">
                        <label for="video">Короткая ссылка на видео</label>
                        <input type="text" class="form-control @error('video') is-invalid @enderror" name="video" id="video" value="{{ $product->video }}">
                        @error('video')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('video') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-4 col-12">
                        <label for="status">Статус</label>

                        <select name="status" id="status" class="form-control @error('status') is-invalid @enderror" required>
                            <option value="1" @if($product->status == 1) selected @endif >В наличии</option>
                            <option value="2" @if($product->status == 2) selected @endif >Скоро в наличии</option>
                            <option value="3" @if($product->status == 3) selected @endif >Под заказ</option>
                        </select>
                        @error('status')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-4 col-1">
                        <input type="radio" class="form-check-input" id="new" name="new_or_special_price" value="1" @if( $product->new_or_special_price == 1 ) checked @endif>
                        <label for="new">Новый</label>
                    </div>

                    <div class="form-group col-lg-4 col-1">
                        <input type="radio" class="form-check-input" id="special_price" name="new_or_special_price" value="2" @if( $product->new_or_special_price == 2 ) checked @endif>
                        <label for="special_price">Спец цена</label>
                    </div>

                    <div class="form-group col-lg-4 col-1">
                        <input type="radio" class="form-check-input" id="null" name="new_or_special_price" value="0">
                        <label for="null">Сбросить</label>
                    </div>

                    <div class="form-group col-12">
                        <p>Размеры</p>
                        <div class="d-flex checkbox mb-3">
                            <input type="checkbox" name="sizes[{{ 0 }}]" id="size-1" value="00" @if(!empty($product->sizes))  @if(in_array("00", $product->sizes, true)) checked @endif @endif >
                            <label for="size-1" class="mr-4">00</label>

                            <input type="checkbox" name="sizes[{{ 1 }}]" id="size-2" value="0" @if(!empty($product->sizes))  @if(in_array("0", $product->sizes, true)) checked @endif @endif >
                            <label for="size-2" class="mr-4">0</label>

                            <input type="checkbox" name="sizes[{{ 2 }}]" id="size-3" value="2" @if(!empty($product->sizes))   @if(in_array("2", $product->sizes)) checked @endif @endif>
                            <label for="size-3" class="mr-4">2</label>

                            <input type="checkbox" name="sizes[{{ 3 }}]" id="size-4" value="4" @if(!empty($product->sizes))   @if(in_array("4", $product->sizes)) checked @endif @endif>
                            <label for="size-4" class="mr-4">4</label>

                            <input type="checkbox" name="sizes[{{ 4 }}]" id="size-5" value="6" @if(!empty($product->sizes))    @if(in_array("6", $product->sizes)) checked @endif @endif>
                            <label for="size-5" class="mr-4">6</label>

                            <input type="checkbox" name="sizes[{{ 5 }}]" id="size-6" value="8" @if(!empty($product->sizes))   @if(in_array("8", $product->sizes)) checked @endif @endif>
                            <label for="size-6" class="mr-4">8</label>

                            <input type="checkbox" name="sizes[{{ 6 }}]" id="size-7" value="10" @if(!empty($product->sizes))   @if(in_array("10", $product->sizes)) checked @endif @endif>
                            <label for="size-7" class="mr-4">10</label>

                            <input type="checkbox" name="sizes[{{ 7 }}]" id="size-8" value="12" @if(!empty($product->sizes))   @if(in_array("12", $product->sizes)) checked @endif @endif>
                            <label for="size-8" class="mr-4">12</label>

                            <input type="checkbox" name="sizes[{{ 8 }}]" id="size-9" value="14" @if(!empty($product->sizes))   @if(in_array("14", $product->sizes)) checked @endif @endif>
                            <label for="size-9" class="mr-4">14</label>

                            <input type="checkbox" name="sizes[{{ 9 }}]" id="size-10" value="16" @if(!empty($product->sizes))   @if(in_array("16", $product->sizes)) checked @endif @endif>
                            <label for="size-10" class="mr-4">16</label>

                            <input type="checkbox" name="sizes[{{ 10 }}]" id="size-11" value="18" @if(!empty($product->sizes))   @if(in_array("18", $product->sizes)) checked @endif @endif>
                            <label for="size-11" class="mr-4">18</label>

                            <input type="checkbox" name="sizes[{{ 11 }}]" id="size-12" value="20" @if(!empty($product->sizes))   @if(in_array("20", $product->sizes)) checked @endif @endif>
                            <label for="size-12" class="mr-4">20</label>
                        </div>

                        <div class="d-flex checkbox mb-3">
                            <input type="checkbox" name="sizes[{{ 12 }}]" id="size-13" value="XXS" @if(!empty($product->sizes))   @if(in_array("XXS", $product->sizes)) checked @endif @endif>
                            <label for="size-13" class="mr-4">XXS</label>

                            <input type="checkbox" name="sizes[{{ 13 }}]" id="size-14" value="XS" @if(!empty($product->sizes))   @if(in_array("XS", $product->sizes)) checked @endif @endif>
                            <label for="size-14" class="mr-4">XS</label>

                            <input type="checkbox" name="sizes[{{ 14 }}]" id="size-15" value="S" @if(!empty($product->sizes))   @if(in_array("S", $product->sizes)) checked @endif @endif>
                            <label for="size-15" class="mr-4">S</label>

                            <input type="checkbox" name="sizes[{{ 15 }}]" id="size-16" value="M" @if(!empty($product->sizes))   @if(in_array("M", $product->sizes)) checked @endif @endif>
                            <label for="size-16" class="mr-4">M</label>

                            <input type="checkbox" name="sizes[{{ 16 }}]" id="size-17" value="L" @if(!empty($product->sizes))   @if(in_array("L", $product->sizes)) checked @endif @endif>
                            <label for="size-17" class="mr-4">L</label>

                            <input type="checkbox" name="sizes[{{ 17 }}]" id="size-18" value="XL" @if(!empty($product->sizes))   @if(in_array("XL", $product->sizes)) checked @endif @endif>
                            <label for="size-18" class="mr-4">XL</label>

                            <input type="checkbox" name="sizes[{{ 18 }}]" id="size-19" value="XXL" @if(!empty($product->sizes))   @if(in_array("XXL", $product->sizes)) checked @endif @endif>
                            <label for="size-19" class="mr-4">XXL</label>

                            <input type="checkbox" name="sizes[{{ 19 }}]" id="size-20" value="XXXL" @if(!empty($product->sizes))   @if(in_array("XXXL", $product->sizes)) checked @endif @endif>
                            <label for="size-20" class="mr-4">XXXL</label>
                        </div>

                        <div class="d-flex checkbox mb-3">
                            <input type="checkbox" name="sizes[{{ 20 }}]" id="size-21" value="32" @if(!empty($product->sizes))   @if(in_array("32", $product->sizes)) checked @endif @endif>
                            <label for="size-21" class="mr-4">32</label>

                            <input type="checkbox" name="sizes[{{ 21 }}]" id="size-22" value="34" @if(!empty($product->sizes))   @if(in_array("34", $product->sizes)) checked @endif @endif>
                            <label for="size-22" class="mr-4">34</label>

                            <input type="checkbox" name="sizes[{{ 22 }}]" id="size-23" value="36" @if(!empty($product->sizes))   @if(in_array("36", $product->sizes)) checked @endif @endif>
                            <label for="size-23" class="mr-4">36</label>

                            <input type="checkbox" name="sizes[{{ 23 }}]" id="size-24" value="38" @if(!empty($product->sizes))   @if(in_array("38", $product->sizes)) checked @endif @endif>
                            <label for="size-24" class="mr-4">38</label>

                            <input type="checkbox" name="sizes[{{ 24 }}]" id="size-25" value="40" @if(!empty($product->sizes))   @if(in_array("40", $product->sizes)) checked @endif @endif>
                            <label for="size-25" class="mr-4">40</label>

                            <input type="checkbox" name="sizes[{{ 25 }}]" id="size-26" value="42" @if(!empty($product->sizes))   @if(in_array("42", $product->sizes)) checked @endif @endif>
                            <label for="size-26" class="mr-4">42</label>

                            <input type="checkbox" name="sizes[{{ 26 }}]" id="size-27" value="44" @if(!empty($product->sizes))   @if(in_array("44", $product->sizes)) checked @endif @endif>
                            <label for="size-27" class="mr-4">44</label>

                            <input type="checkbox" name="sizes[{{ 27 }}]" id="size-28" value="46" @if(!empty($product->sizes))   @if(in_array("46", $product->sizes)) checked @endif @endif>
                            <label for="size-28" class="mr-4">46</label>

                            <input type="checkbox" name="sizes[{{ 28 }}]" id="size-29" value="48" @if(!empty($product->sizes))   @if(in_array("48", $product->sizes)) checked @endif @endif>
                            <label for="size-29" class="mr-4">48</label>

                            <input type="checkbox" name="sizes[{{ 29 }}]" id="size-30" value="50" @if(!empty($product->sizes))   @if(in_array("50", $product->sizes)) checked @endif @endif>
                            <label for="size-30" class="mr-4">50</label>

                        </div>
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="designer_id">Выберите дизайнера</label>
                        <select name="designer_id" id="designer_id" class="form-control">
                            @foreach($designers as $designer)
                                <option value="{{ $designer->id }}" @if($designer->id == $product->designer->id) selected @endif >{{ $designer->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-lg-6 col-12">
                        <label for="category_id">Выберите категорию</label>
                        <select name="category_id" id="category_id" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if($category->id == $product->category->id) selected @endif >{{ $category->title }}</option>
                                @if(!empty($category->subcategories))
                                    @foreach($category->subcategories as $subcategory)
                                        <option value="{{ $subcategory->id }}" @if($subcategory->id == $product->category->id) selected @endif > ● {{ $subcategory->title }}</option>
                                    @endforeach
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-lg-8 col-12">
                        <label for="colors">Цвета</label>
                        <input type="text" class="form-control @error('colors') is-invalid @enderror" name="colors" id="colors" value="{{ $product->colors }}">
                        @error('colors')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('colors') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-4 col-12">
                        <input type="checkbox" class="form-check-input" id="show_recommendations" name="show_recommendations" value="1" @if($product->show_recommendations) checked @endif>
                        <label for="show_recommendations">ХРАНЕНИЕ/ХИМЧИСТКА/РЕСТАВРАЦИЯ</label>
                    </div>

                    <div class="form-group col-sm-12">
                        <label for="description">Описание</label>
                        <textarea name="description" id="description" maxlength="500" class="form-control tinymce @error('description') is-invalid @enderror" cols="10" style="min-height: 160px">
                            {{ $product->description }}
                        </textarea>
                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @enderror
                        <p class="text-center">Для адаптации картинки по ширине экрана - 100% - auto</p>
                    </div>


                </div>
                <hr>

                <div class="row pb-5">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn-save">Сохранить</button>
                    </div>
                </div>

            </form>

        </div>
    </section>

@stop

