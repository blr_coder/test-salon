@extends('layouts.admin')

@section('title')
    <title>Admin panel - Продукты</title>
@endsection

@section('content')


    <section id="admin" class="admin-articles">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>

                            <li class="breadcrumb-item active" aria-current="page">Продукты</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-sm-4 text-right mt-2">
                    @if( count(\App\Models\Category::all()) > 0 )
                        <a href="{{ action('Admin\ProductController@create') }}" class="btn-primary">Добавить продукт</a>
                    @else
                        <a href="{{ action('Admin\CategoryController@index') }}" class="btn-primary">Добавьте хотя бы одну категорию!</a>
                    @endif
                </div>
            </div>
        </div>

        {{--        SearchSearchSearchSearchSearchSearchSearch--}}
        <div class="card mb-3">
            <div class="card-header">Поиск</div>
            <div class="card-body">

                <form action="?" method="GET">
                    <div class="row">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="id" class="col-form-label">ID</label>
                                <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="title" class="col-form-label">Название</label>
                                <input id="title" class="form-control" name="title" value="{{ request('title') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="model" class="col-form-label">Модель(артикул)</label>
                                <input id="model" class="form-control" name="model" value="{{ request('model') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="designer" class="col-form-label">Дизайнер</label>
                                <input id="designer" class="form-control" name="designer" value="{{ request('designer') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="category" class="col-form-label">Категория(подкатегория)</label>
                                <input id="category" class="form-control" name="category" value="{{ request('category') }}">
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="col-form-label">&nbsp;</label><br>
                                <button type="submit" class="btn-sm btn-primary">Поиск</button>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label class="col-form-label">&nbsp;</label><br>
                                <a href="/admin/products" class="btn-sm btn-success">Сбросить</a>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>
        {{--        SearchSearchSearchSearchSearchSearchSearch--}}

        <div class="container-fluid">
            <div class="row pt-2">

                <div class="col-sm-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 50px">ID</th>
                            <th style="width: 150px">Изображение 1</th>
                            <th style="width: 150px">Изображение 2</th>
                            <th style="width: 150px">Изображение 3</th>
                            <th style="width: 150px">Видео</th>
                            <th>Название</th>
                            <th>Модель</th>
                            <th>Цена</th>
                            <th>Цена со скидкой</th>
                            <th>Дизайнер</th>
                            <th>Категория</th>
                            <th>Статус</th>
                            <th>НОВИНКА</th>
                            <th>Спец цена</th>
                            <th>Цвета</th>
                            <th>Размеры</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td style="width: 5%" class="border">{{ $product->id }}</td>
                                <td style="width: 10%" class="border"><img src="/storage/uploaded_images/products/{{ $product->image_1 }}" class="img-fluid" alt="{{ $product->title }}"></td>
                                <td style="width: 10%" class="border"><img src="/storage/uploaded_images/products/{{ $product->image_2 }}" class="img-fluid" alt="{{ $product->title }}"></td>
                                <td style="width: 10%" class="border"><img src="/storage/uploaded_images/products/{{ $product->image_3 }}" class="img-fluid" alt="{{ $product->title }}"></td>
                                <td style="width: 5%" class="border">

                                    @if(!empty($product->video))
                                        <a href="https://www.youtube.com/watch?v={{ Str::after($product->video, 'youtu.be/') }}=0s" target="_blank">
                                            <img src="https://img.youtube.com/vi/{{ Str::after($product->video, 'youtu.be/') }}/hqdefault.jpg" class="img-fluid">
                                        </a>
                                    @else
                                        без видео
                                    @endif


                                </td>
                                <td style="width: 5%" class="border">{{ mb_strtoupper($product->title) }}</td>
                                <td style="width: 5%" class="border">{{ mb_strtoupper($product->model) }}</td>
                                <td style="width: 5%" class="border">{{ mb_strtoupper($product->price) }}</td>
                                <td style="width: 5%" class="border">{{ mb_strtoupper($product->discount_price) }}</td>
                                <td style="width: 5%" class="border">{{ mb_strtoupper($product->designer->title) }}</td>
                                <td style="width: 5%" class="border">{{ $product->category->title }}</td>
                                <td style="width: 5%" class="border">
                                    @switch ( $product->status )
                                        @case(1)
                                            В наличии
                                            @break
                                        @case(2)
                                            Скоро в наличии
                                            @break
                                        @case(3)
                                            Под заказ
                                            @break
                                    @endswitch
                                </td>
                                <td class="border"> @if($product->isNew()) НОВИНКА @endif </td>
                                <td class="border"> @if($product->isSpecialPrice()) СПЕЦ ЦЕНА! @endif </td>
                                <td class="border"> {{ $product->colors }} </td>
                                <td class="border">
                                    @if(!empty($product->sizes))
                                        @foreach($product->sizes as $size)
                                            {{ $size }},
                                        @endforeach
                                    @endif
                                </td>
                                <td class="border">
                                    <div class="row">

                                        <div class="col-12">
                                            <a href="{{ action('Admin\ProductController@edit', [$product]) }}">
                                                <button class="btn-edit">Редактировать</button>
                                            </a>
                                        </div>

                                        <div class="col-12">
                                            <form method="post" action="{{ action('Admin\ProductController@delete', [$product]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <div>
                                                    <button type="submit" onclick="return confirm('Вы уверены, что хотите удалить продукт?')" class="btn-remove w-100">Удалить</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </section>
@endsection
