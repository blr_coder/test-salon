@extends('layouts.admin')

@section('content')

    <section id="admin" class="admin-articles">
        <div class="container">
            <div class="row">
                <div class="col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AlbumController@index') }}">Фотоосессии</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Загрузка фотографий в фотосессию <strong>"{{ $album->title }}"</strong></li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row">

                    <div class="col-lg-8 col-12">

                            <div class="row">
                                @forelse($album->photos as $photo)
                                    <div class="col-xl-3 col-lg-6 col-12 mb-4 mt-2">
                                        <img src="/storage/uploaded_images/photos/small/{{ $photo->image_small }}" class="img-fluid" alt="{{ $photo->title }}">

                                        <a href="{{ action('Admin\PhotoController@delete', [$photo]) }}">
                                            <button class="btn-remove">удалить</button>
                                        </a>
                                    </div>
                                @empty
                                    <div>Не одной фотографии ещё не загружено.</div>
                                @endforelse
                            </div>

                    </div>

                    <div class="col-lg-4 col-12">

                        <form action="#" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="row">

                                <div class="col-lg-12 ">

                                    <div class="input-group">
                                        <input type="text" class="form-control @error('title') is-invalid @enderror" placeholder="Наименование фотографий" name="title" aria-describedby="addon-wrapping">
                                        @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group mt-4 mb-4">
                                        <label for="images">Изображения (рекомендуем не более 10 шт. за один раз)</label>
                                        <input type="file" class="form-control-file" required id="images" name="images[]"
                                               multiple >
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6 col-12"><button type="submit" class="btn-create">Загрузить выбранные</button></div>
                                            <div class="col-lg-6 col-12 text-right"><a href="{{ action('Admin\PhotoController@deleteAll', [$album]) }}" class="btn-remove">Удалить все</a></div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

            </div>
        </div>

@endsection
