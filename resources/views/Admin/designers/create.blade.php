@extends('layouts.admin')

@section('content')

    <section id="admin" class="admin-articles">
        <div class="container">

            <div class="row pt-5">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>
                            <li class="breadcrumb-item"><a href="{{ action('Admin\DesignerController@index') }}">Дизайнеры</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Добавление дизайнера</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-sm-12 text-center">
                    <h5>Добавление дизайнера на ресурсе</h5>
                </div>
            </div>

            <form action="{{ action('Admin\DesignerController@store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="col-lg-12 col-12">
                        <label for="title">Наименование</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" maxlength="190">
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @enderror
                    </div>

                </div>
                <hr>

                <div class="row pb-5">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn-create">Создать</button>
                    </div>
                </div>

            </form>

        </div>
    </section>

@stop

