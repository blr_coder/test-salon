@extends('layouts.admin')

@section('content')

    <section id="admin" class="admin-articles">
        <div class="container">

            <div class="row pt-5">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>
                            <li class="breadcrumb-item"><a href="{{ action('Admin\DesignerController@index') }}">Дизайнеры</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Редактирование дизайнера</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-sm-12 text-center">
                    <h5>Редактирование дизайнера {{ $designer->title }}</h5>
                </div>
            </div>

            <form action="{{ action('Admin\DesignerController@update', [$designer]) }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="form-group col-lg-12 col-12">
                        <label for="title">Наименование</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" maxlength="190" required value="{{ $designer->title }}">
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group col-lg-12 col-12">
                        <label for="slug">SLUG</label>
                        <input type="text" class="form-control @error('slug') is-invalid @enderror" id="slug" name="slug" maxlength="190" required value="{{ $designer->slug }}">
                        @error('slug')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('slug') }}</strong>
                            </span>
                        @enderror
                    </div>

                </div>
                <hr>

                <div class="row pb-5">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn-save">Сохранить</button>
                    </div>
                </div>

            </form>

        </div>
    </section>

@stop

