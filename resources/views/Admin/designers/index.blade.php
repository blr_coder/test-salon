@extends('layouts.admin')

@section('title')
    <title>Admin panel - Дизайнеры</title>
@endsection

@section('content')


    <section id="admin" class="admin-articles">

        <div class="container-fluid">
            <div class="row pt-5">
                <div class="col-md-8 col-12 text-md-left text-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-transparent">
                            <li class="breadcrumb-item"><a href="{{ action('Admin\AdminController@index') }}">Администратор</a></li>

                            <li class="breadcrumb-item active" aria-current="page">Дизайнеры</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-sm-4 text-right mt-3">
                    <a href="{{ action('Admin\DesignerController@create') }}" class="btn-primary">Добавить дизайнера</a>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row pt-4">

                <div class="col-sm-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>SLUG</th>
                            <th>Товары</th>
                            <th class="text-center">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($designers as $designer)
                            <tr>
                                <td style="width: 30%">{{ mb_strtoupper($designer->title) }}</td>
                                <td style="width: 30%">{{ $designer->slug }}</td>
                                <td style="width: 20%">Товары...</td>
                                <td style="width: 20%">
                                    <div class="d-flex justify-content-between">

                                        <div>
                                            <a href="{{ action('Admin\DesignerController@edit', [$designer]) }}">
                                                <button class="btn-edit">Редактировать</button>
                                            </a>
                                        </div>

                                        <div>
                                            <form method="post" action="{{ action('Admin\DesignerController@delete', [$designer]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <div>
                                                    <button type="submit" onclick="return confirm(`Вы уверены, что хотите удалить дизайнера?`)" class="btn-remove">Удалить</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </section>
@endsection
