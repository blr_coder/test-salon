<!-- Modal 1 -->
<div class="modal fade about-modal" id="aboutModal1" tabindex="-1" role="dialog" aria-labelledby="aboutModal1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h5 class="text-center">SHERRI HILL</h5>

                <p>
                    Платья Sherri Hill
                    <span class="font-light">
                        – для смелых, уверенных в себе девушек, любящих дорогой блеск и качественные ткани. Найти аналоги бренда невозможно, поэтому сегодня он очень высоко ценится. Sensation – является единственным официальным представителем дизайнера в стране.
                    </span>
                </p>
                <p>
                    В настоящее время семейный бизнес Sherri Hill насчитывает более 1000 магазинов, продающих бренд в более чем 52 странах.
                </p>

                <p>
                    Sherri Hill отпраздновал своё 11-летие! Из них вот уже 6 лет наш салон единственный официально представляет этот бренд на территории Беларуси!
                </p>

                <p class="main-color">
                    В семейном магазине, в котором продавалось абсолютно все, Шерри работала с 8 лет. В первый же день она украсила манекены драпированной легкой тканью, создав свои первые наряды. Такой прием привлекал внимание покупателей, благодаря чему продажи значительно выросли.
                </p>

                <p class="main-color">
                    Шерри Хилл, в отличие от многих дизайнеров с такой же историей из детства, продолживших творческий путь самоучками, действительно серьёзно изучала дизайн одежды
                    в университете Оклахомы.
                </p>

                <p class="main-color">
                    После университета Хилл работала для разных производителей в мире вечерней моды. Она начала завоевывать признание в мире моды, когда победители конкурса Мисс Америка и Мисс Вселенная выигрывали
                    в платьях, которые она разработала.
                </p>
                <p class="main-color">
                    Шерри Хилл работала 12 лет в качестве приглашённого дизайнера Jovani Fashions, помогая запустить бренд.
                </p>
                <p class="main-color">
                    Она покинула Jovani в 2008 году, чтобы начать свою собственную линию вечерних нарядов. В этом же году на свет появляется полноценный бренд Sherri Hill.
                </p>
                <p class="main-color">
                    Sherri Hill - официальный дизайнер и спонсор самого масштабного конкурса красоты в мире - Мисс Вселенная.
                </p>



            </div>

        </div>
    </div>
</div>


<!-- Modal 2 -->
<div class="modal fade about-modal" id="aboutModal2" tabindex="-1" role="dialog" aria-labelledby="aboutModal2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h5 class="text-center">TARIK EDIZ</h5>

                <p>
                    Tarik Ediz
                    <span class="font-light">
                        – дизайнер, завоевавший доверие клиентов на 5 континентах и в 95 странах мира. Особенную популярность турецкий бренд приобрел в Италии благодаря высококачественным тканям и фурнитуре, а также соответствию европейским стандартам пошива платьев.
                    </span>
                </p>

                <p class="main-color mb-2">
                    100% ручная работа
                </p>
                <p class="main-color mb-2">
                    Собственная фабрика в Стамбуле
                </p>
                <p class="main-color">
                    Возможность отшить платье под заказ в любом размере
                </p>

                <p>
                    Tarik Ediz
                    <span class="font-light">
                        стал столь близок европейцам за счет сдержанных коллекций платьев, отвечающих последним тенденциям вечерней моды.
                    </span>
                </p>
                <p class="font-light">
                    Платья производятся вручную опытными мастерами, сочетают
                    в себе женственную утонченность, наполненную сексуальной чувственностью, и изысканность высокой моды.
                </p>
                <p class="font-light">
                    Владелец дома моды, дизайнер Tarik Ediz (Тарик Эдиз), лично руководит процессом их создания, чтобы гарантировать высочайшее качество и уникальность каждого изделия, выпущенного под своим именем. Непревзойденный кутюрье признаёт, что создание нарядов для женщин является не только его призванием, но и смыслом жизни: «Дарить женщинам счастье – что может быть лучше?».
                </p>



            </div>

        </div>
    </div>
</div>


<!-- Modal 3 -->
<div class="modal fade about-modal" id="aboutModal3" tabindex="-1" role="dialog" aria-labelledby="aboutModal3" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h5 class="text-center">TERANI COUTURE</h5>

                <p class="font-light">
                    Платья Terani Сouture носят женщины по всему миру. Независимо от стиля, формы тела, размера женщины, в салоне можно найти красивейшие коллекции платьев из специальных линеек «мама невесты и жениха».
                </p>

                <p>
                    Terani Сouture
                    <span class="font-light">
                        начала свою деятельность как компания по производству женских костюмов в швейном районе Нью-Йорка в 1990 году, а затем превратилась в бренд для особых случаев. И представила коллекции, которые в настоящее время варьируются от выпускных платьев, коктейльных, платьев подружки невесты до платьев мамы невесты.
                    </span>
                </p>

                <p>
                    Платья Terani Сouture
                    <span class="font-light">
                        украшены изысканной вышивкой, камнями. Американские стилисты часто разыскивают этот бренд для того, чтобы одеть общественных деятелей и влиятельных лиц в социальных сетях, чтобы украсить престижные мероприятия на красной ковровой дорожке, театрализованные представления и страницы глянца. Самое главное, что эти неподвластные времени вещи постоянно сопровождают женщин в их особых случаях, делая Terani Couture незабываемой частью их бесценных моментов.
                    </span>
                </p>



            </div>

        </div>
    </div>
</div>
