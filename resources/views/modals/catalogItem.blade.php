<!-- Modal 1 -->
<div class="modal fade" id="catalogModal1" tabindex="-1" role="dialog" aria-labelledby="catalogModal1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h5 class="text-center">Хранение</h5>

                <p>
                    Легкие, короткие:
                    <span class="font-light">
                        хранить в шкафу сложенными,
                        в фирменном пакете, если при таком хранении не образуется заломов.

                    </span>
                </p>


                <p>
                    Длинные:
                    <span class="font-light">
                        вешаем только за специальные петли, храним в чехле, либо в фирменном пакете , а после мероприятия, когда петли срезаны, храним на широких бретельках, либо горизонтально. В висящем положении места с утяжелениями могут растянуться или обвиснуть. Место должно быть сухим, температура воздуха не высокая, удаленность от источников тепла и прямых солнечных лучей.
                    </span>
                </p>

                <p>
                    Даже самый качественный  камушек может растрескаться от неправильного хранения или рассыпаться под действием сырости. Такие  изделия не любят резких перепадов температуры.
                </p>


                <p>
                    Аксессуары:
                    <span class="font-light">
                        ремни, шарфы, накидки — храним отдельно.
                    </span>
                </p>


            </div>

        </div>
    </div>
</div>



<!-- Modal 2 -->
<div class="modal fade" id="catalogModal2" tabindex="-1" role="dialog" aria-labelledby="catalogModal2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h5 class="text-center">Химчистка</h5>

                <p>
                    Лучше всего обратиться в химчистку.
                </p>

                <p class="font-light">
                    Если на платье есть пайетки, вышивка, бисер, нужна химчистка «green earth», в ней щадящие компоненты, которые не повредят украшения.
                </p>

                <p>
                    Обратите внимание специалиста на украшения. Неправильная обработка, может привести к потекам.
                </p>

                <p>
                    Клиентам напоминаем, что за вашим платьем ухаживаем мы!
                </p>

                <p class="font-weight-light">
                    Химчистка и реставрация лежат на наших плечах. С вас требуется только правильное хранение ! А тем, кто собирается стать нашим клиентом, советуем сделать это поскорее, чтобы такие мелочи (а на деле — не мелочи) совсем не заботили вас!
                </p>


            </div>

        </div>
    </div>
</div>



<!-- Modal 3 -->
<div class="modal fade" id="catalogModal3" tabindex="-1" role="dialog" aria-labelledby="catalogModal3" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h5 class="text-center">Реставрация</h5>

                <p class="font-weight-light">
                    Если у вас оторвались камушки или ослабились нити, то нет ничего страшного в том, чтобы самостоятельно их подшить, так как большинство платьев вышиты вручную.
                </p>

                <p>
                    <span class="main-color">Важно !</span> Нитка должна быть либо телесного цвета, либо в тон других нитей на платье. Также можно воспользоваться тонкой леской.
                </p>

                <p>
                    Иголка должна быть швейной, очень тонкой, чтобы не повредить деликатную ткань.
                </p>

                <p class="font-weight-light">
                    Недостающие или потерявшиеся камушки вы можете попросить в салоне, в котором вы приобретали платье.
                    Уверены, что вам смогут предложить альтернативу, даже в случае, если подобных камней уже нет. Мелкую фурнитуру: крючки, цепочки, кнопки-невидимки можно найти в магазинах типа "метро".
                </p>


            </div>

        </div>
    </div>
</div>
