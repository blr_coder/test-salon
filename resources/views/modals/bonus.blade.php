<!-- Modal 1 -->
<div class="modal fade" id="bonusModal1" tabindex="-1" role="dialog" aria-labelledby="bonusModal1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h5 class="text-center font-weight-bold">Хотите приобрести подарочный сертификат?</h5>
                <p class="font-italic mb-5 text-center">Заполните форму и укажите сумму сертификата</p>


                <form action="">
                    <div class="form-group">
                        <input id="name" type="text" class="input-name" required>
                        <label for="name">Имя</label>
                    </div>

                    <div class="form-group">
                        <input id="phone" type="text" class="phone-name input-mask" required>
                        <label for="phone">Телефон</label>
                    </div>

                    <div class="form-group">
                        <input id="count" type="text" class="count-name" required>
                        <label for="count">Введите сумму сертификата</label>
                    </div>

                    <div class="form-group">
                        <textarea name="comment" id="comment" class="comment-name"></textarea>
                        <label for="comment">Пожелания</label>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="main-btn">Отправить</button>
                    </div>
                </form>



            </div>

        </div>
    </div>
</div>


<!-- Modal 2 -->
<div class="modal fade" id="bonusModal2" tabindex="-1" role="dialog" aria-labelledby="bonusModal2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h5 class="text-center font-weight-bold">дисконтная программа “sensation”</h5>
                <p class="text-center mb-4">Возможность получить скидки до 20% от покупки в бутике </p>

                <div class="mb-4">
                    <p class="main-color mb-2 font-weight-bold">
                        Первая покупка:
                    </p>

                    <p>
                        <span class="d-block">Платье стоимостью до 2000 рублей - выдается дисконтная карта на 3 %.</span>
                        <span class="d-block">Платье стоимостью от 2000 рублей - выдается дисконтная карта на 5 %.</span>
                    </p>

                </div>

                <div class="mb-4">
                    <p class="main-color font-weight-bold mb-2">
                        После третьей покупки:
                    </p>

                    <p>
                        Карта клиента 3% заменяется на  карту 5%.
                    </p>

                </div>


                <div class="mb-4">
                    <p class="main-color font-weight-bold mb-2">
                        После шестой покупки:
                    </p>

                    <p>
                        Карта клиента заменяется на карту 10%.
                    </p>

                </div>


                <div class="mb-4">
                    <p class="main-color font-weight-bold mb-2">
                        После девятой покупки:
                    </p>

                    <p>
                        Карта клиента заменяется на карту 15%.
                    </p>

                </div>


                <p class="main-color mb-4">
                    После преодоления рубежа в 15 платьев, учитывая общую сумму покупок, принимается решение о замене карты на именную с постоянной 20% скидкой.
                </p>

                <p class="font-light">
                    Скидка не распространяется на акционные товары и товары по специальным ценам.
                    Карта является именной и не может передаваться третьим лицам. Организатор дисконтной программы оставляет за собой право изменять условия программы.
                </p>





            </div>

        </div>
    </div>
</div>


