@extends('layouts.layout')

@section('content')

    @include('partial.nav')

    <section id="main-banner">
        <div class="container-left">
            <div id="carouselMainBanner" class="carousel slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselMainBanner" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselMainBanner" data-slide-to="1"></li>
                    <li data-target="#carouselMainBanner" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="/img/main/banner-1.jpg" class="d-block w-100" alt="Салон вечерней моды Sensation">
                        <h2 class="banner">своим появлением произведи сенсацию!</h2>
                    </div>
                    <div class="carousel-item">
                        <img src="/img/main/banner-2.jpg" class="d-block w-100" alt="Салон вечерней моды Sensation">
                        <h2 class="banner">более 500 платьев
                            в салоне ежедневно</h2>

                    </div>
                    <div class="carousel-item">
                        <img src="/img/main/banner-3.jpg" class="d-block w-100" alt="Салон вечерней моды Sensation">
                        <h2 class="banner">
                            Платья от ведущих мировых дизайнеров
                        </h2>
                    </div>
                </div>

                <a href="https://www.instagram.com/sensation.by/" target="_blank" class="insta-box d-flex align-items-center">
                    <span class="mr-3 d-none d-sm-inline-block">
                        Подпишитесь на наш инстаграм
                    </span>
                    <img src="/svg/insta-grey.svg" alt="">
                </a>

            </div>
        </div>
    </section>


    <section class="mb-80">
        <div class="container">
            <div class="w-60 text-center mb-100">
                <h1 class="to-span">
                    <img src="/svg/sensation.svg" alt="">
                    <br>
                    <span class="title-line">
                    Платья от ведущих мировых дизайнеров
                    </span>
                </h1>

                <p class="font-light">
                    В нашем салоне вы сможете найти ежедневно более 500 платьев с широким размерным рядом
                    в наличии. Все наши платья новые и без повреждений, так как бутик не предоставляет возможнсти проката. Нет времени забрать платье? Вы доставим его Вам домой.
                </p>
            </div>



            <div class="slick-box__container position-relative">

                <div class="wow overlay" data-wow-duration="2s" data-wow-delay="15ms" data-wow-offset="10"></div>

                <div class="slick-box">
                    <div class="slick-box__card">
                        @include('partial.card')
                    </div>
                    <div class="slick-box__card">
                        @include('partial.card')
                    </div>
                    <div class="slick-box__card">
                        @include('partial.card')
                    </div>
                    <div class="slick-box__card">
                        @include('partial.card')
                    </div>
                </div>
            </div>



        </div>
    </section>

    <section class="section-grey pt-80 pb-80">
        <div class="container">
            <div class="w-60">
                <h3 class="text-center mb-50">
                    <span class="title-line">
                        миссия
                    </span>

                </h3>
            </div>

            <div class="row align-items-end justify-content-center position-relative">

                <div class="wow overlay-grey" data-wow-duration="2s" data-wow-delay="15ms" data-wow-offset="10"></div>

                <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <p class="d-inline-block d-md-block">
                        Наша миссия заключается в продвижении идеи соблюдения дресс-кода, в формировании особого отношения к вечернему наряду и к культуре приобретения платья.
                        Деятельность салона направлена на то, чтобы в день своего важного торжественного события женщина чувствовала себя уверенной в своей неотразимости!
                    </p>
                    <img src="/img/main/missia-1.jpg" alt="Салон вечерней моды Sensation - наша миссия" class="w-100">
                </div>

                <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <img src="/img/main/missia-2.jpg" alt="Салон вечерней моды Sensation - наша миссия" class="w-100">
                    <p class="mb-0 mt-3 d-inline-block d-md-block">
                        Мы не просто продаем платья, мы создаем новую культуру вечерней моды, и в нашем бутике вы найдёте платья от самых известных мировых дизайнеров. Лучшие из мира кутюр разделяют нашу миссию и дают нам возможность быть их единственными эксклюзивными представителями на территории Беларуси
                    </p>
                </div>

                <div class="col-md-6 col-lg-4 position-relative">
                    <div class="special-text text-uppercase">
                        Своим появлением произведи сенсацию!
                    </div>
                    <img src="/img/main/missia-3.jpg" alt="Салон вечерней моды Sensation - наша миссия" class="w-100">
                </div>

            </div>
        </div>

    </section>

    <section class="pt-80 pb-80">
        <div class="container">
            <div class="slick-box__container position-relative">

                <div class="wow overlay" data-wow-duration="2s" data-wow-delay="15ms" data-wow-offset="10"></div>

                <div class="slick-box">
                    <div class="slick-box__card">
                        @include('partial.card')
                    </div>
                    <div class="slick-box__card">
                        @include('partial.card')
                    </div>
                    <div class="slick-box__card">
                        @include('partial.card')
                    </div>
                    <div class="slick-box__card">
                        @include('partial.card')
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="pt-80 pb-80 adv-section">
        <div class="container">
            <div class="w-60">
                <h3 class="text-center mb-50">
                    <span class="title-line">
                        преимущества
                    </span>

                </h3>
            </div>

            <div class="row pt-sm-4">
                <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 text-center text-lg-left">
                    <object type="image/svg+xml" data="/svg/adv-1.svg"  class="mb-30">
                    </object>
                    <h4>сервис</h4>
                    <ul class="text-left">
                        <li>- подгонка любой сложности на месте;</li>
                        <li>- возможность привезти платье под заказ;</li>
                        <li>- химчистка и пост-обслуживание платья.</li>
                    </ul>
                </div>

                <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 text-center text-lg-left">
                    <object type="image/svg+xml" data="/svg/adv-2.svg"  class="mb-30">
                    </object>
                    <h4>Удобное местоположение</h4>
                    <ul class="text-left">
                        <li>- центр города;</li>
                        <li>- бесплатная и удобная парковка.</li>
                    </ul>
                </div>

                <div class="col-sm-6 col-lg-3 mb-4 mb-sm-0 text-center text-lg-left">
                    <object type="image/svg+xml" data="/svg/adv-3.svg"  class="mb-30">
                    </object>
                    <h4>Большой салон</h4>
                    <ul class="text-left">
                        <li>- самая большая примерочная в Европе;</li>
                        <li>- профессиональный подиум;</li>
                        <li>- возможность прийти большой компанией.</li>
                    </ul>
                </div>

                <div class="col-sm-6 col-lg-3 text-center text-lg-left">
                    <object type="image/svg+xml" data="/svg/adv-4.svg"  class="mb-30">
                    </object>
                    <h4>Примерка</h4>
                    <ul class="text-left">
                        <li>- примерка без записи; </li>
                        <li>- примерка по записи в закрытом салоне;</li>
                        <li>- персональная работа стилиста над образом.</li>
                    </ul>
                </div>
            </div>
        </div>

    </section>


    <section class="section-grey pt-80 pb-80">
        <div class="container">

            <div class="row align-items-end justify-content-center position-relative">

                <div class="wow overlay-grey" data-wow-duration="2s" data-wow-delay="15ms" data-wow-offset="10"></div>

                <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <p>
                        Каждый день мы совершенствуемся: учимся новому, ездим по миру, собирая знания. Мы хорошо знаем мир вечерней моды, а это не только платья.
                    </p>
                    <img src="/img/main/missia-4.jpg" alt="Салон вечерней моды Sensation - наша миссия" class="w-100">
                </div>

                <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <img src="/img/main/missia-5.jpg" alt="Салон вечерней моды Sensation - наша миссия" class="w-100">
                    <p class="mb-0 mt-3">
                        У нас также есть украшения, сумочки, клатчи. Мы знаем, где найти правильное белье и самые удобные туфли.
                        Мы посоветуем вам лучшего парикмахера и визажиста.
                    </p>
                </div>

                <div class="col-md-6 col-lg-4 position-relative">
                    <div class="special-text">
                        Мы готовы к любому громкому событию! А вы приготовьтесь влюбиться в платье и потерять голову!
                    </div>
                    <img src="/img/main/missia-6.jpg" alt="Салон вечерней моды Sensation - наша миссия" class="w-100">
                </div>

            </div>
        </div>

    </section>

    <section class="pt-80 pb-80">
        <div class="container">
            <div class="w-60">
                <h3 class="text-center mb-50">
                    <span class="title-line">
                        бонусы и сертификаты
                    </span>
                </h3>
            </div>

            <div class="row justify-content-center position-relative">

                <div class="wow overlay" data-wow-duration="2s" data-wow-delay="15ms" data-wow-offset="10"></div>

                <div class="col-sm-10 col-md-8 col-lg-6">

                    <div class="info__bonus-card">
                        <p class="text-uppercase">Не знаете, что подарить?</p>
                        <p class="font-light fs-16 mb-0">
                            Помогите своей подруге почувствовать себя уверенной и неотразимой с помощью нашего подарочного сертификата на приобретение  дизайнерского платья от салона Sensation.
                        </p>
                    </div>

                    <div class="bonus-card">
                        <img src="/img/main/bonus-1.png" alt="Салон вечерней моды Sensation - сертификат" class="w-100">
                        <button type="button" data-toggle="modal" data-target="#bonusModal1" class="main-btn main-btn-1">Купить</button>
                    </div>

                </div>

                <div class="col-sm-10 col-md-8 col-lg-6">

                    <div class="info__bonus-card">
                        <p class="text-uppercase">
                            ХОтите РЕГУЛЯРНО ПОЛУЧАТЬ БОНУСЫ ОТ НАШЕГО САЛОНА?
                        </p>
                        <p class="font-light fs-16 mb-0">
                            Принимайте участие в нашей бонусной программе и получите дисконтную карту, которая даст Вам возможность приобретать платья от мировых дизайнеров по выгодной цене!
                        </p>
                    </div>


                    <div class="bonus-card">
                        <img src="/img/main/bonus-2.png" alt="Салон вечерней моды Sensation - дисконтная карта" class="w-100">
                        <button type="button" data-toggle="modal" data-target="#bonusModal2" class="main-btn main-btn-2">Участвовать</button>
                    </div>


                </div>
            </div>
        </div>

    </section>

    @include('modals.bonus')
    @include('partial.footer')

@endsection
