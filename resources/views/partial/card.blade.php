    <div class="item--card">
        <a href="/item/test">
            <div class="top-container">
                <img src="/storage/uploaded_images/products/0oZP3ZgfPuA3ZvhDRs9xvg8p0TkuAleE.jpg" alt="Test !!!" class="w-100">
            </div> <div class="bottom-container">
                <div class="d-sm-flex justify-content-between">
                    <div class="mb-2 mb-sm-0">
                        <h4>Test !!!</h4>
                        <div class="in-stock-box">В наличии</div> <!----> <!---->
                    </div>
                    <div class="with-discount text-sm-right">
                        <div class="old-price">15</div>
                        <div class="new-price">4 BYN</div>
                    </div>
                </div>
            </div>
            <div class="main-btn w-100">Подробнее</div>
        </a>
    </div>
