<footer class="pt-80 pb-80">
    <div class="container position-relative">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-6 mb-5 mb-xl-0">
                <div class="row justify-content-between">
                    <div class="col-sm-5">
                        <nav class="mb-40">
                            <ul>
                                <li><a href="/catalog">Каталог</a></li>
                                <li><a href="/about">О салоне</a></li>
                            </ul>
                            <ul>
                                <li><a href="/photo_sessions">Фотосессии</a></li>
                                <li><a href="/news">Новости</a></li>
                            </ul>
                        </nav>

                        <form action="" class="email-form">
                            <label for="email-footer">Узнавайте первыми о новой коллекции</label>
                            <input id="email-footer" name="email-footer" type="email" placeholder="E-mail">
                            <button type="submit" class="main-btn w-100 mt-4">Подписаться</button>
                        </form>
                    </div>

                    <div class="col-sm-5">
                        <div class="contact-box">
                            <div class="mb-40">
                                <a href="tel:+375296701930" class="d-flex align-items-center justify-content-center justify-content-sm-start mb-2 mb-xl-3"><img src="/svg/call-pink.svg" alt="" class="mr-2">+375 (29) 670-19-30</a>
                                <a href="tel:+375447902020" class="d-flex align-items-center justify-content-center justify-content-sm-start"><img src="/svg/call-pink.svg" alt="" class="mr-2">+375 (44) 790-20-20</a>
                            </div>

                            <div>
                                <div class="mb-3 d-flex align-items-start">
                                    <img src="/svg/place-pink.svg" alt="" class="mr-2">
                                    <div>
                                        <span class="d-block mb-2">г. Минск, пр-т. Победителей 65,</span>
                                        <span>ТЦ “Замок”, этаж 4, уровень 6</span>
                                    </div>
                                </div>

                                <form action="/catalog" method="get">
                                    <input type="search" class="search-line" placeholder="Найти по артикулу.." name="search-tag" @if(!empty(request('search-tag'))) value="{{ request('search-tag') }}" @endif>
                                    <button type="submit" class="search-btn"><img src="/svg/search.svg" alt=""></button>
                                </form>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-6">

                <div class="insta-box">
                    <a href="https://www.instagram.com/sensation.by/" target="_blank">Подпишись на наш инстаграм <img src="/svg/insta-pink.svg" alt="" class="ml-2"></a>
                </div>
                <div class="row">
                    <div class="col-3">
                        <a href="https://www.instagram.com/sensation.by/" target="_blank">
                            <img src="/img/insta/insta-1.jpg" alt="" class="w-100">
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="https://www.instagram.com/sensation.by/" target="_blank">
                            <img src="/img/insta/insta-2.jpg" alt="" class="w-100">
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="https://www.instagram.com/sensation.by/" target="_blank">
                            <img src="/img/insta/insta-3.jpg" alt="" class="w-100">
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="https://www.instagram.com/sensation.by/" target="_blank">
                            <img src="/img/insta/insta-4.jpg" alt="" class="w-100">
                        </a>
                    </div>
                </div>

            </div>
        </div>

        <div class="dev-box">
            <a href="https://ilavista.by" target="_blank">Разработка и дизайн - Ilavista</a>
        </div>
    </div>
</footer>


<div id="btn-to-up" class="link-to-article">
    <a href="#app"><img src="/svg/arrow-left-white.svg" alt=""></a>
</div>
