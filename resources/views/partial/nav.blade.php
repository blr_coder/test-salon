<header class="desctop-header">

    {{--FIRST LINE--}}
    <div class="first-line__nav">

        <div class="container">
            <div class="d-flex align-items-end justify-content-between">
                <div class="d-flex align-items-center">
                    {{--Main LOGO--}}
                    <div class="logo-container">
                        <a href="/">
                            <img src="/img/icons/main-logo-310.svg" alt="" class="main-logo">
                        </a>
                    </div>

                    <nav>
                        <ul>
                            <li class="@yield('active-class-1')"><a href="/catalog">Каталог</a></li>
                            <li class="@yield('active-class-2')"><a href="/about">О салоне</a></li>
                            <li class="@yield('active-class-3')"><a href="/photo_sessions">Фотосессии</a></li>
                            <li class="@yield('active-class-4')"><a href="/news">Новости</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="contact-box d-flex mb-2">
                    <div class="mr-5 d-flex flex-column justify-content-top">
                        <a href="tel:+375296701930" class="d-flex align-items-center mb-2"><img src="/svg/call.svg" alt="" class="mr-2">+375 (29) 670-19-30</a>
                        <a href="tel:+375447902020" class="d-flex align-items-center"><img src="/svg/call.svg" alt="" class="mr-2">+375 (44) 790-20-20</a>
                    </div>

                    <div>
                        <div class="mb-2 d-flex align-items-start">
                            <img src="/svg/place.svg" alt="" class="mr-1">
                            <div>
                                <span class="d-block mb-2">г. Минск, пр-т. Победителей 65,</span>
                                <span>ТЦ “Замок”, этаж 4, уровень 6</span>
                            </div>
                        </div>

                        <form action="/catalog" method="get">
                            <input type="search" class="search-line" placeholder="Найти по артикулу.." name="search-tag" @if(!empty(request('search-tag'))) value="{{ request('search-tag') }}" @endif>
                            <button type="submit" class="search-btn"><img src="/svg/search.svg" alt=""></button>
                        </form>
                    </div>

                </div>

            </div>
        </div>


    </div>

    {{--SECOND LINE--}}
    <div class="second-line__nav @yield('background-color')">

        <nav-links></nav-links>


    </div>


</header>




{{--/*****************************SCROLL NAVIGATION*******************/--}}
<div id="scroll-nav">
    <a href="/"><img src="/img/icons/main-logo-scroll.svg" alt=""></a>
    <nav class="first-line__nav pb-0">
        <ul class="mt-0">
            <li class="@yield('active-class-1')"><a href="/catalog">Каталог</a></li>
            <li class="@yield('active-class-2')"><a href="/about">О салоне</a></li>
            <li class="@yield('active-class-3')"><a href="/photo_sessions">Фотосессии</a></li>
            <li class="@yield('active-class-4')"><a href="/news">Новости</a></li>
        </ul>
    </nav>
    <div class="contact-box d-flex">
        <div class="mr-5 d-flex flex-column justify-content-top">
            <a href="tel:+375296701930" class="d-flex align-items-center mb-1"><img src="/svg/call.svg" alt="" class="mr-2">+375 (29) 670-19-30</a>
            <a href="tel:+375447902020" class="d-flex align-items-center"><img src="/svg/call.svg" alt="" class="mr-2">+375 (44) 790-20-20</a>
        </div>

        <div>
            <div class="mb-1 d-flex align-items-start">
                <img src="/svg/place.svg" alt="" class="mr-1 mt-1">
                <div>
                    <span class="d-block mb-1">г. Минск, пр-т. Победителей 65,</span>
                    <span>ТЦ “Замок”, этаж 4, уровень 6</span>
                </div>
            </div>

            <form action="/catalog" method="get">
                <input type="search" class="search-line" placeholder="Найти по артикулу.." name="search-tag" @if(!empty(request('search-tag'))) value="{{ request('search-tag') }}" @endif>
                <button type="submit" class="search-btn"><img src="/svg/search.svg" alt=""></button>
            </form>
        </div>

    </div>
</div>




{{--/*****************************MOBILE*******************/--}}
<header class="mobile-header">
    <div class="container d-flex justify-content-between align-items-center">
        <div class="logo-container">
            <a href="/">
                <img src="/img/icons/main-logo-90.svg" alt="" class="main-logo">
            </a>
        </div>


        <div class="contact-box">
            <div>
                <a href="tel:+375296701930" class="d-flex align-items-center mb-2"><img src="/svg/call.svg" alt="" class="mr-2">+375 (29) 670-19-30</a>
                <a href="tel:+375447902020" class="d-flex align-items-center"><img src="/svg/call.svg" alt="" class="mr-2">+375 (44) 790-20-20</a>
            </div>


        </div>



        <button type="button" class="burger-btn"><img src="/img/icons/menu.svg" alt=""></button>

    </div>

</header>


<div id="mobile-menu">

    <div class="container">
        <div class="contact-box mb-4">
            <div class="mb-3">
                <img src="/svg/place.svg" alt="">
                <span>ТЦ “Замок”, этаж 4, уровень 6</span>
            </div>

            <form action="/catalog" method="get">
                <input type="search" class="search-line" placeholder="Найти по артикулу.." name="search-tag" @if(!empty(request('search-tag'))) value="{{ request('search-tag') }}" @endif>
                <button type="submit" class="search-btn"><img src="/svg/search.svg" alt=""></button>
            </form>
        </div>

        <nav>
            <ul>
                <li><a href="/catalog">Каталог</a></li>
                <li><a href="/about">О салоне</a></li>
                <li><a href="/photo_sessions">Фотосессии</a></li>
                <li><a href="/news">Новости</a></li>
            </ul>
        </nav>
    </div>



</div>
