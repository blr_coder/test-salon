@extends('layouts.layout')

@section('active-class-4', 'active')

@section('content')

    @include('partial.nav')

    <section class="article-page">
        <div id="main-banner" class="mb-0">

            <img src="/storage/uploaded_images/articles/{{ $article->image_for_desktop }}" alt="" class="w-100 ">

            <div class="info-box text-center w-60">
                <h1>{{ $article->title }}</h1>
                <p class="mb-50 d-none d-md-block">{{ $article->preview }}</p>

                <div class="link-to-article">
                    <a href="#article-show">
                        <span></span>
                    </a>
                </div>


            </div>

        </div>
    </section>

    <section class="pb-80" id="article-show">
        <div class="container">
            <div class="breadcrumb-nav">
                <a href="/">Главная</a>
                <a href="{{ action('ArticleController@index') }}">Новости</a>
                <a href="{{ action('ArticleController@show', [$article->slug]) }}" class="active">{{ $article->title }}</a>
            </div>

            <div class="row pt-3 justify-content-between">
                <div class="col-md-7 mb-4 mb-md-0">
                    <h5 class="mb-30 article-title">
                        {{ $article->title }}
                    </h5>

                    <div class="font-light article-description mb-5">
                        {!! Illuminate\Support\Str::limit($article->description, $limit = 20000, $end = '...') !!}
                    </div>

                    <a href="{{ action('ArticleController@index') }}">
                        <div class="btn-to-back">Назад в раздел новостей</div>
                    </a>
                </div>

                <div class="col-md-5 col-lg-4">
                    <div class="d-flex flex-column flex-sm-row flex-md-column">

                        @foreach($otherArticles as $otherArticle)

                            <div class="mb-4 mr-0 mr-sm-4 mr-md-0">
                                <a href="{{ action('ArticleController@show', [$otherArticle->slug]) }}">
                                    <div class="card">
                                        <div class="img-container">
                                            <img src="/storage/uploaded_images/articles/small/{{ $otherArticle->image_for_desktop_small }}" alt="" class="w-100">
                                        </div>
                                        <h5>{{ $otherArticle->title }}</h5>
                                        <p>
                                            {{ $otherArticle->preview }}
                                            <span>Подробнее</span>
                                        </p>

                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>

                </div>
            </div>



        </div>
    </section>

    @include('partial.footer')

@endsection
