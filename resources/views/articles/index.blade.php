@extends('layouts.layout')

@section('active-class-4', 'active')

@section('content')

    @include('partial.nav')

    @if(!empty($articles[0]))
        <section class="article-page">
            <div id="main-banner">

                <img src="/storage/uploaded_images/articles/{{ $articles[0]->image_for_desktop }}" alt="{{ $articles[0]->title }}" class="w-100 ">

                <div class="info-box text-center w-60">
                    <h3>{{ $articles[0]->title }}</h3>
                    <p class="mb-50 d-none d-md-block">{{ $articles[0]->preview }}</p>

                    <a href="{{ action('ArticleController@show', [$articles[0]->slug]) }}">
                        <div class="btn-second">
                            Читать подробнее
                        </div>
                    </a>

                </div>

            </div>
        </section>
    @endif

    <section class="pb-80">
        <div class="container">
            <div class="breadcrumb-nav">
                <a href="/">Главная</a>
                <a href="/news" class="active">Новости</a>
            </div>

            <div class="w-60">
                <h1 class="text-center mb-50">
                    <span class="title-line">
                        новости
                    </span>

                </h1>
            </div>

            <div class="row">

                @foreach($articles as $article)
                    <div class="col-md-6 col-xl-4 mb-4">
                        <a href="{{ action('ArticleController@show', [$article->slug]) }}">
                            <div class="card">
                                <div class="img-container">
                                    <img src="/storage/uploaded_images/articles/small/{{ $article->image_for_desktop_small }}" alt="{{ $article->title }}" class="w-100">
                                </div>
                                <h5>{{ $article->title }}</h5>
                                <p>
                                    {{ $article->preview }}
                                    <span class="btn-next">Подробнее</span>
                                </p>

                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

    @include('partial.footer')

@endsection
