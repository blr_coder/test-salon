@extends('layouts.app')

@section('content')

    <div class="text-center">
        <img src="/img/logo_big.png" alt="">
        <h1>403</h1>
        <h3>Доступ запрещён</h3>
        <a href="/" class="btn btn-primary">Вернуться на главную</a>
    </div>

@stop
