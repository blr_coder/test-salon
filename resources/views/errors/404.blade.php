@extends('layouts.layout')


@section('content')

    @include('partial.nav')

    <section class="text-center pt-80 pb-80">

        <h3 class="mb-40">Такой страницы не существует!</h3>
        <p class="mb-5">Возможно, она была удалена.</p>
        <a href="/" class="main-btn">Вернуться на главную</a>

    </section>

    @include('modals.aboutModals')
    @include('partial.footer')

@endsection
