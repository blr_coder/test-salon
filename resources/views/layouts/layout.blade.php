<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @php($assets_version = 5)


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700|Roboto:300,400,500&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="/css/app.css?v={{ $assets_version }}">

    @if(!empty($page_title))
        <title>{{ $page_title }}</title>

        <meta property="og:title" content="{{ $page_title }}"/>
        <meta name="twitter:title" content="{{ $page_title }}"/>
    @else
        <title>Салон вечерней моды - Sensation</title>
    @endif

    @if(!empty($page_description))
        <meta name="description" content="{{ $page_description }}">
        <meta property="og:description" content="{{ $page_description }}"/>
        <meta name="twitter:description" content="{{ $page_description }}"/>
        <meta name="twitter:text:description" content="{{ $page_description }}"/>
    @endif



</head>
<body>

<div id="preview" style="position: fixed; transition: 0.9s ease; z-index: 99999; background: #fff; left: 0; top:0; bottom: 0; width:100%">
    <span class="load">
        <img src="/img/icons/main-logo-310.svg" alt="" style="">
    </span></div>
<main id="app">
    @yield('content')
</main>


<script src="/js/app.js?v={{ $assets_version }}"></script>
<script src="/js/wow.min.js"></script>
<script>
    new WOW().init();
</script>

</body>
</html>
