<?php

///////////BACK-END///////////BACK-END///////////BACK-END///////////BACK-END///////////BACK-END///////////BACK-END//////

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/** Admin-panel */
Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' =>['auth', 'can:adminPanel'],
    ],

    function (){
        /** Admin */
        Route::get('/', 'AdminController@index')->name('admin.home');

        /** Articles */
        Route::get('articles', 'ArticleController@index');
        Route::get('articles/create', 'ArticleController@create');
        Route::post('articles/store', 'ArticleController@store');
        Route::get('articles/{article}/edit', 'ArticleController@edit');
        Route::post('articles/{article}/update', 'ArticleController@update');
        Route::delete('articles/{article}/delete', 'ArticleController@delete');

        /** Albums */
        Route::get('albums', 'AlbumController@index');
        Route::get('albums/create', 'AlbumController@create');
        Route::post('albums/store', 'AlbumController@store');
        Route::get('albums/{album}/edit', 'AlbumController@edit');
        Route::post('albums/{album}/update', 'AlbumController@update');
        Route::delete('albums/{album}/delete', 'AlbumController@delete');

        /** Photos */
        Route::get('albums/{album}/photos', 'PhotoController@index');
        Route::post('albums/{album}/photos', 'PhotoController@store');
        Route::get('photos/{photo}/delete', 'PhotoController@delete');
        Route::get('photos/{album}/delete_all', 'PhotoController@deleteAll');

        /** Categories */
        Route::get('categories', 'CategoryController@index');
        Route::get('categories/create', 'CategoryController@create');
        Route::post('categories/store', 'CategoryController@store');
        Route::get('categories/{category}/edit', 'CategoryController@edit');
        Route::post('categories/{category}/update', 'CategoryController@update');
        Route::delete('categories/{category}/delete', 'CategoryController@delete');

        /** Designers */
        Route::get('designers', 'DesignerController@index');
        Route::get('designers/create', 'DesignerController@create');
        Route::post('designers/store', 'DesignerController@store');
        Route::get('designers/{designer}/edit', 'DesignerController@edit');
        Route::post('designers/{designer}/update', 'DesignerController@update');
        Route::delete('designers/{designer}/delete', 'DesignerController@delete');

        /** Products */
        Route::get('products', 'ProductController@index');
        Route::get('products/create', 'ProductController@create');
        Route::post('products/store', 'ProductController@store');
        Route::get('products/{product}/edit', 'ProductController@edit');
        Route::post('products/{product}/update', 'ProductController@update');
        Route::delete('products/{product}/delete', 'ProductController@delete');


        /** Logs */
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    }
);

///////////BACK-END///////////BACK-END///////////BACK-END///////////BACK-END///////////BACK-END///////////BACK-END//////

Route::get('/', 'PagesController@index');

/** News */
Route::get('news', 'ArticleController@index');
Route::get('news/{slug}', 'ArticleController@show');


/** Photo_sessions */
Route::get('photo_sessions', 'AlbumController@index');
Route::get('photo_sessions/{slug}', 'AlbumController@show');



Route::get('/about', 'PagesController@about');



//For navigation
Route::get('/catalog/platya-v-nalichii', 'CatalogController@static');
Route::get('/catalog/novinki', 'CatalogController@static');
Route::get('/catalog/specialnaya-cena', 'CatalogController@static');


/** Catalog */
Route::get('catalog/{category?}/{sub_category?}', 'CatalogController@index');

/** Items */
Route::get('item/{slug}', 'ItemController@show');


Route::get('/thanks', function () {
    return view('info.thanks');
});
