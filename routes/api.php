<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** API/Products */
Route::post('products', 'API\ProductApiController@get')->name('api.getProducts');

/** API/Designers */
Route::post('designers', 'API\DesignerApiController@get')->name('api.getDesigners');

/** API/mainCategories */
Route::post('main_categories', 'API\CategoryApiController@getMainCategories')->name('api.getMainCategories');

/** API/subCategories */
Route::post('sub_categories', 'API\CategoryApiController@getSubCategories')->name('api.getSubCategories');
